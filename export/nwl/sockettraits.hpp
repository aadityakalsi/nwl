/*! sockettraits.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_SOCKETTRAITS_HPP
#define NWL_SOCKETTRAITS_HPP

#include <nwl/error.hpp>
#include <nwl/exports.h>
#include <nwl/types.hpp>

namespace nwl {

#if defined(_WIN32)
using NativeSocket = std::intptr_t;
static const NativeSocket InvalidSocket = ~static_cast<NativeSocket>(0);
#else
using NativeSocket = int;
static const NativeSocket InvalidSocket = -1;
#endif

// ShutDownKind
struct ShutDownKind
{
    enum Type
    {
        Read = 0x0,
        Write = 0x1,
        Both = 0x2
    };
};

// MilliSecond, Second
using MilliSecond = std::uint64_t;
using Second = std::uint32_t;

// SocketTraits (platform specific)
struct NWL_API SocketTraits
{
    using SizeType = std::uint32_t;

    static void initSocketUsage();

    static void close(ErrorCode* err, NativeSocket sock);

    static bool isValid(NativeSocket sock);

    static SizeType read(ErrorCode* err,
                         NativeSocket sock,
                         void* buff,
                         SizeType size);

    static SizeType write(ErrorCode* err,
                          NativeSocket sock,
                          const void* buff,
                          SizeType size);

    static void shutDown(ErrorCode* err,
                         NativeSocket sock,
                         ShutDownKind::Type kind);

    static void setBlocking(ErrorCode* err, NativeSocket sock, bool block);

    static void setKeepAlive(ErrorCode* err,
                             NativeSocket sock,
                             Second kaTime);

    static void setNoDelay(ErrorCode* err, NativeSocket sock, bool noDelay);

    static void setSendTimeout(ErrorCode* err,
                               NativeSocket sock,
                               MilliSecond timeout);

    static ErrorCode lastError();

    static bool isBlockingError(const ErrorCode& err);
};

// Inline definitions
inline bool SocketTraits::isValid(NativeSocket sock)
{
    return sock != InvalidSocket;
}

inline bool isBlockingError(const ErrorCode& err)
{
    return SocketTraits::isBlockingError(err);
}

} // namespace nwl

#endif /*NWL_SOCKETTRAITS_HPP*/
