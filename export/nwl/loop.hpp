/*! loop.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_LOOP_HPP
#define NWL_LOOP_HPP

#include <nwl/event.hpp>
#include <nwl/types.hpp>

#include <memory>

namespace nwl {

class NWL_API Loop
{
  public:
    // CREATORS
    Loop();
    ~Loop();

    // MANIPULATORS
    void stop();

    // REGISTER/UNREGISTER
    SocketToken registerSocket(Socket sock,
                               SocketObserver* listener,
                               SocketEvent::Type events);
    SocketToken registerSocket(Socket sock,
                               SocketObserver* listener,
                               int events);
    Socket deregisterSocket(SocketToken id);

    TimerToken registerTimeEvent(TimerEvent::MilliSecond delay,
                                 TimerObserver* listener);
    bool deregisterTimeEvent(TimerToken id);

    void run(ErrorCode* err);

  private:
#ifdef _MSC_VER
#    pragma warning(disable : 4251)
#endif
    struct Impl;
    std::unique_ptr<Impl> d_impl;
#ifdef _MSC_VER
#    pragma warning(default : 4251)
#endif

    bool resizeSocketSet(size_t size);
};

// inlines

inline SocketToken Loop::registerSocket(Socket s, SocketObserver* l, int ev)
{
    auto sev = static_cast<SocketEvent::Type>(ev);
    assert(sev == SocketEvent::Read || sev == SocketEvent::Write ||
           sev == (SocketEvent::Read | SocketEvent::Write) ||
           sev == SocketEvent::None);
    return registerSocket(std::move(s), l, sev);
}

} // namespace nwl

#endif /*NWL_LOOP_HPP*/
