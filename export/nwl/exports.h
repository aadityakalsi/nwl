/*! exports.h */

#ifndef _NWL_EXPORTS_H_
#define _NWL_EXPORTS_H_

#if defined(USE_NWL_STATIC)
#    define NWL_API
#elif defined(_WIN32) && !defined(__GCC__)
#    ifdef BUILDING_NWL_SHARED
#        define NWL_API __declspec(dllexport)
#    else
#        define NWL_API __declspec(dllimport)
#    endif
#    ifndef _CRT_SECURE_NO_WARNINGS
#        define _CRT_SECURE_NO_WARNINGS
#    endif
#else
#    ifdef BUILDING_NWL_SHARED
#        define NWL_API __attribute__((visibility("default")))
#    else
#        define NWL_API
#    endif
#endif

#if defined(__cplusplus)
#    define NWL_EXTERN_C extern "C"
#    define NWL_C_API NWL_EXTERN_C NWL_API
#else
#    define NWL_EXTERN_C
#    define NWL_C_API NWL_API
#endif

#endif /*_NWL_EXPORTS_H_*/
