/*! filefactory.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILEFACTORY_HPP
#define NWL_FILEFACTORY_HPP

#include <nwl/error.hpp>
#include <nwl/exports.h>
#include <nwl/file.hpp>
#include <nwl/types.hpp>

namespace nwl {

// FileMode
struct FileMode
{
    enum Type
    {
        Read = 0x1,
        Write = 0x2,
        Both = 0x3,
        Append = 0x4,
    };
};

// FileUsage
struct FileUsage
{
    enum Type
    {
        Sequential = 0x1,
        Random = 0x2
    };
};

// FileFactory
struct NWL_API FileFactory
{
    using SizeType = std::uint64_t;

    static File open(ErrorCode* err,
                     const String& file,
                     FileMode::Type mode = FileMode::Read,
                     FileUsage::Type intent = FileUsage::Sequential);

    static File create(ErrorCode* err,
                       const String& file,
                       FileMode::Type mode = FileMode::Both,
                       FileUsage::Type intent = FileUsage::Sequential);

    static void erase(ErrorCode* err, const String& file);

    static SizeType size(ErrorCode* err, const String& file);

    static File fromNative(NativeFile f);
};

} // namespace nwl

#endif /*NWL_FILEFACTORY_HPP*/
