/*! file.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILE_HPP
#define NWL_FILE_HPP

#include <nwl/exports.h>
#include <nwl/filetraits.hpp>

namespace nwl {

class NWL_API File
{
  private:
    // DATA
    NativeFile d_fd;

  public:
    File() noexcept;
    ~File() noexcept;

    File(File&& rhs) noexcept;
    File& operator=(File&& rhs) noexcept;

    File(const File&) = delete;
    File& operator=(const File&) = delete;

    // ACCESSORS
    bool valid() const;
    NativeFile native() const;

    using SizeType = FileTraits::SizeType;
    SizeType write(ErrorCode* err, const void* data, SizeType count) const;
    SizeType read(ErrorCode* err, void* data, SizeType count) const;

    using OffsetType = FileTraits::OffsetType;

    OffsetType tell(ErrorCode* err) const;
    void seek(ErrorCode* err, OffsetType off) const;

    // MANPIULATORS
    void close(ErrorCode* err);

  private:
    friend struct FileFactory;
    File(NativeFile) noexcept;
};

} // namespace nwl

#endif /*NWL_FILE_HPP*/
