/*! socketfactory.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_SOCKETFACTORY_HPP
#define NWL_SOCKETFACTORY_HPP

#include <nwl/socket.hpp>
#include <nwl/types.hpp>

#include <ostream>

namespace nwl {

struct Endpoint
{
    String address;
    Port portNum;
};

struct NWL_API SocketFactory
{
    // CLASS METHODS
    static Socket createTcpServer(ErrorCode* err,
                                  const String& host,
                                  Port p,
                                  int backlog);
    static Socket createTcp6Server(ErrorCode* err,
                                   const String& host,
                                   Port p,
                                   int backlog);
    static Socket accept(ErrorCode* err,
                         Endpoint* from,
                         const Socket& lstnSock);

    static Socket connect(ErrorCode* err, const String& host, Port p);
    static Socket connectNonBlocking(ErrorCode* err,
                                     const String& host,
                                     Port p);
    static Socket connectNonBlockingBind(ErrorCode* err,
                                         const String& host,
                                         Port p,
                                         const String& bindAddr);

    static void createSocketPair(ErrorCode* err,
                                 Socket* sockA,
                                 Socket* sockB);

    static void peerEndpoint(ErrorCode* err,
                             Endpoint* ep,
                             const Socket& peer);
    static void localEndpoint(ErrorCode* err,
                              Endpoint* ep,
                              const Socket& sock);
    static void hostName(ErrorCode* err, String* host);

  private:
    static Socket connectOpts(ErrorCode* err,
                              const String& host,
                              Port p,
                              const String& bindAddr,
                              bool nonBlock);
};

NWL_API std::ostream& operator<<(std::ostream& os, const Endpoint& ep);

} // namespace nwl

#endif /*NWL_SOCKETFACTORY_HPP*/
