/*! filetraits.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILETRAITS_HPP
#define NWL_FILETRAITS_HPP

#include <nwl/error.hpp>
#include <nwl/exports.h>
#include <nwl/types.hpp>

namespace nwl {

#if defined(_WIN32)
using NativeFile = void*;
static const NativeFile InvalidFile = reinterpret_cast<NativeFile>(-1);
#else
using NativeFile = int;
static const NativeFile InvalidFile = -1;
#endif

// FileTraits (platform specific)
struct NWL_API FileTraits
{
    using OffsetType = std::int64_t;
    using SizeType = std::uint32_t;

    static void close(ErrorCode* err, NativeFile fd);

    static bool isValid(NativeFile fd);

    static SizeType read(ErrorCode* err,
                         NativeFile fd,
                         void* buff,
                         SizeType size);

    static SizeType write(ErrorCode* err,
                          NativeFile fd,
                          const void* buff,
                          SizeType size);

    static OffsetType tell(ErrorCode* err, NativeFile fd);

    static void seek(ErrorCode* err, NativeFile fd, OffsetType off);
};

// Inline definitions
inline bool FileTraits::isValid(NativeFile fd)
{
    return fd != InvalidFile;
}

} // namespace nwl

#endif /*NWL_FILETRAITS_HPP*/
