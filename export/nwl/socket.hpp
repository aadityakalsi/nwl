/*! socket.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_SOCKET_HPP
#define NWL_SOCKET_HPP

#include <nwl/sockettraits.hpp>

namespace nwl {

class NWL_API Socket
{
  private:
    // DATA
    NativeSocket d_fd;

  public:
    // CREATORS
    Socket() noexcept;
    ~Socket() noexcept;

    Socket(Socket&& rhs) noexcept;
    Socket& operator=(Socket&& rhs) noexcept;

    Socket(const Socket&) = delete;
    Socket& operator=(const Socket&) = delete;

    // ACCESSORS
    bool valid() const;
    NativeSocket native() const;

    using SizeType = SocketTraits::SizeType;
    SizeType write(ErrorCode* err, const void* data, SizeType count) const;
    SizeType read(ErrorCode* err, void* data, SizeType count) const;

    SizeType writeAll(ErrorCode* err, const void* data, SizeType count) const;
    SizeType readAll(ErrorCode* err, void* data, SizeType count) const;

    // MANPIULATORS
    void shutDown(ErrorCode* err, ShutDownKind::Type kind) const;
    void close(ErrorCode* err);

    void setBlocking(ErrorCode* err, bool on) const;
    void setKeepAlive(ErrorCode* err, Second kaTime) const;
    void setNoDelay(ErrorCode* err, bool on) const;
    void setSendTimeout(ErrorCode* err, MilliSecond timeout) const;

  private:
    Socket(NativeSocket) noexcept;

    friend struct SocketFactory;
    friend class Loop;
};

} // namespace nwl

#endif /*NWL_SOCKET_HPP*/
