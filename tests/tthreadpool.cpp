/*! tthreadpool.cpp */

#include "defs.h"

#include <nwl/threadpool.hpp>

#include <atomic>

#include <chrono>
#include <cstdio>
#include <iostream>
#include <list>

void tpool(void)
{
    std::atomic<int> val{ 0 };
    const int N = 2000;
    {
        nwl::ThreadPool tp;
        for (int i = 0; i < N; ++i) {
            tp.enqueue([&val]() -> void { ++val; });
        }
    }
    testThat(val == N);
}

void tpoolOverheadEmpty(void)
{
    const int N = std::getenv("N") ? std::atoi(std::getenv("N")) : (int)1e3;
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        {
            nwl::ThreadPool tp;
            for (int i = 0; i < N; ++i) {
                tp.enqueue([]() -> void {});
            }
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout << "Time per empty function call (parallel): "
                  << std::chrono::duration_cast<std::chrono::microseconds>(
                       t1 - t0)
                       .count()
                  << "us. ";
    }
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        std::list<std::function<void()>> list;
        for (int i = 0; i < N; ++i) {
            list.push_back([]() -> void {});
        }
        for (int i = 0; i < N; ++i) {
            list.front()();
            list.pop_front();
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout << "Time per empty function call (serial)  : "
                  << std::chrono::duration_cast<std::chrono::microseconds>(
                       t1 - t0)
                       .count()
                  << "us. ";
    }
}

void tpoolOverheadFor(void)
{
    const int N = std::getenv("N") ? std::atoi(std::getenv("N")) : (int)1e3;
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        {
            nwl::ThreadPool tp;
            for (int i = 0; i < N; ++i) {
                tp.enqueue([]() -> void {
                    for (int i = 0; i < 10000; ++i) {
                        // nothing
                    }
                });
            }
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout << "Time per for-loop function call (parallel): "
                  << std::chrono::duration_cast<std::chrono::microseconds>(
                       t1 - t0)
                       .count()
                  << "us. ";
    }
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        std::list<std::function<void()>> list;
        for (int i = 0; i < N; ++i) {
            list.push_back([]() -> void {
                for (int i = 0; i < 10000; ++i) {
                    // nothing
                }
            });
        }
        for (int i = 0; i < N; ++i) {
            list.front()();
            list.pop_front();
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout << "Time per for-loop function call (serial)  : "
                  << std::chrono::duration_cast<std::chrono::microseconds>(
                       t1 - t0)
                       .count()
                  << "us. ";
    }
}

void tpoolOverheadFread(void)
{
    const int N = std::getenv("N") ? std::atoi(std::getenv("N")) : (int)1e3;
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        {
            nwl::ThreadPool tp(2);
            for (int i = 0; i < N; ++i) {
                tp.enqueue([]() -> void {
                    FILE* f = std::fopen(__FILE__, "r");
                    assert(f);
                    char buff[1024];
                    auto s = std::fread(buff, 1, 1024, f);
                    testThat(s > 0);
                    std::fclose(f);
                });
            }
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout
          << "Time per file-open-read-close function call (parallel): "
          << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0)
               .count()
          << "us. ";
    }
    std::cout << "\n";
    {
        auto t0 = std::chrono::high_resolution_clock::now();
        std::list<std::function<void()>> list;
        for (int i = 0; i < N; ++i) {
            list.push_back([]() -> void {
                FILE* f = std::fopen(__FILE__, "r");
                assert(f);
                char buff[1024];
                auto s = std::fread(buff, 1, 1024, f);
                testThat(s > 0);
                std::fclose(f);
            });
        }
        for (int i = 0; i < N; ++i) {
            list.front()();
            list.pop_front();
        }
        auto t1 = std::chrono::high_resolution_clock::now();
        std::cout
          << "Time per file-open-read-close function call (serial)  : "
          << std::chrono::duration_cast<std::chrono::microseconds>(t1 - t0)
               .count()
          << "us. ";
    }
}

setupSuite(tthreadpool_cpp)
{
    addTest(tpool);
    addTest(tpoolOverheadEmpty);
    addTest(tpoolOverheadFor);
    addTest(tpoolOverheadFread);
}
