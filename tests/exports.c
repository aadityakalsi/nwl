/*! exports.c */

#include "defs.h"

#include "nwl/exports.h"

#ifdef NWL_C_API
#    define VER_DEF_FOUND 1
#else
#    define VER_DEF_FOUND 0
#endif

void apiMacro(void)
{
    testThat(1 == VER_DEF_FOUND);
}

setupSuite(exports)
{
    addTest(apiMacro);
}
