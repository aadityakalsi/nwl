/*! tfile.cpp */

#include "defs.h"

#include <nwl/file.hpp>
#include <nwl/filefactory.hpp>

void fileCreateWriteCloseOpenReadCloseErase(void)
{
    nwl::String name = "myTempFile.txt";
    nwl::ErrorCode err;

    nwl::File wf = nwl::FileFactory::create(&err, name);
    testThat(!err);
    testThat(wf.valid());
    testThat(wf.write(&err, "TEST", 4) == 4);
    testThat(!err);
    wf.close(&err);
    testThat(!err);

    testThat(nwl::FileFactory::size(&err, name) == 4);
    testThat(!err);

    nwl::File rf = nwl::FileFactory::open(&err, name);
    testThat(!err);
    testThat(rf.valid());
    char buff[5] = { '\0' };
    testThat(rf.read(&err, buff, 4) == 4);
    testThat(!err);
    testThat(std::strcmp(buff, "TEST") == 0);
    rf.close(&err);
    testThat(!err);

    nwl::FileFactory::erase(&err, name);
    testThat(!err);
}

setupSuite(tfile_cpp)
{
    addTest(fileCreateWriteCloseOpenReadCloseErase);
}
