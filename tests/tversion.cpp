/*! tversion.cpp */

#include "defs.h"

#include <nwl/version.hpp>

#include <cstring>

void checkVersion(void)
{
    testThat(std::strcmp(nwl::version(), "0.1.1") == 0);
}

setupSuite(tversion_cpp)
{
    addTest(checkVersion);
}
