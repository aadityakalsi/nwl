/*! tsocketfactory.cpp */

#include "defs.h"

#include <iostream>
#include <nwl/socketfactory.hpp>
#include <thread>

using SockFact = nwl::SocketFactory;

void createTcpServer(void)
{
    nwl::ErrorCode err;
    auto s = SockFact::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());
    nwl::Endpoint ep;
    SockFact::localEndpoint(&err, &ep, s);
    testThat(!err);
    std::cout << ep << " ";
    s.close(&err);
    testThat(!err);
}

void createTcp6Server(void)
{
    nwl::ErrorCode err;
    auto s = SockFact::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());
    nwl::Endpoint ep;
    SockFact::localEndpoint(&err, &ep, s);
    testThat(!err);
    std::cout << ep << " ";
    s.close(&err);
    testThat(!err);
}

void connectAndTalkBlocking(void)
{
    nwl::ErrorCode err;
    auto s = SockFact::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());

    std::thread t([]() -> void {
        nwl::ErrorCode err;
        auto c = SockFact::connect(&err, "127.0.0.1", 8080);
        testThat(c.valid());
        testThat(!err);
        char buff[] = "xxx";
        testThat(c.write(&err, "foo", 3) == 3);
        testThat(!err);
        testThat(c.read(&err, buff, 3) == 3);
        testThat(!err);
        testThat(std::strcmp(buff, "bar") == 0);
        c.close(&err);
        testThat(!err);
    });

    nwl::Endpoint ep;
    auto c = SockFact::accept(&err, &ep, s);
    testThat(c.valid());
    testThat(!err);

    char buff[] = "xxx";
    testThat(c.read(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "foo") == 0);
    testThat(c.write(&err, "bar", 3) == 3);
    testThat(!err);
    t.join();
    testThat(c.read(&err, buff, 1) == 0);
    testThat(!err);
}

void endPoints(void)
{
    nwl::ErrorCode err;
    auto s = SockFact::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());

    nwl::Endpoint srv;
    nwl::Endpoint srvCli;
    nwl::Endpoint cli;
    nwl::Endpoint cliSrv;

    std::thread t([&cli, &cliSrv]() -> void {
        nwl::ErrorCode err;
        auto c = SockFact::connect(&err, "127.0.0.1", 8080);
        testThat(c.valid());
        testThat(!err);
        char buff[] = "xxx";
        testThat(c.write(&err, "foo", 3) == 3);
        testThat(!err);
        testThat(c.read(&err, buff, 3) == 3);
        testThat(!err);
        testThat(std::strcmp(buff, "bar") == 0);

        SockFact::peerEndpoint(&err, &cliSrv, c);
        testThat(!err);
        SockFact::localEndpoint(&err, &cli, c);
        testThat(!err);

        c.close(&err);
        testThat(!err);
    });

    nwl::Endpoint ep;
    auto c = SockFact::accept(&err, &ep, s);
    testThat(c.valid());
    testThat(!err);

    char buff[] = "xxx";
    testThat(c.read(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "foo") == 0);
    testThat(c.write(&err, "bar", 3) == 3);
    testThat(!err);

    SockFact::peerEndpoint(&err, &srvCli, c);
    testThat(!err);
    SockFact::localEndpoint(&err, &srv, c);
    testThat(!err);

    t.join();
    testThat(c.read(&err, buff, 1) == 0);
    testThat(!err);

    std::cout << "Server: " << srv << " ";

    testThat(srv.address == cliSrv.address);
    testThat(srv.portNum == cliSrv.portNum);
    testThat(srv.address == "127.0.0.1");
    testThat(srv.portNum == 8080);

    testThat(cli.address == srvCli.address);
    testThat(cli.portNum == srvCli.portNum);
}

void connectAndTalkMakeNonBlock(void)
{
    nwl::ErrorCode err;
    auto s = SockFact::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());
    s.setBlocking(&err, false);
    testThat(!err);

    std::thread t([]() -> void {
        nwl::ErrorCode err;
        auto c = SockFact::connect(&err, "127.0.0.1", 8080);
        testThat(c.valid());
        testThat(!err);
        c.setBlocking(&err, false);
        testThat(!err);
        char buff[] = "xxx";
        testThat(c.writeAll(&err, "foo", 3) == 3);
        testThat(!err);
        testThat(c.readAll(&err, buff, 3) == 3);
        testThat(!err);
        testThat(std::strcmp(buff, "bar") == 0);
        c.close(&err);
        testThat(!err);
    });

    nwl::Endpoint ep;
    nwl::Socket c;
    while (!(c = SockFact::accept(&err, &ep, s)).valid()) {
    }
    testThat(c.valid());
    testThat(!err);
    c.setBlocking(&err, false);
    testThat(!err);

    char buff[] = "xxx";
    testThat(c.readAll(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "foo") == 0);
    testThat(c.writeAll(&err, "bar", 3) == 3);
    testThat(!err);
    t.join();
    testThat(c.read(&err, buff, 1) == 0);
    testThat(!err);
}

void socketPair(void)
{
    nwl::Socket A, B;
    nwl::ErrorCode err;
    nwl::SocketFactory::createSocketPair(&err, &A, &B);
    testThat(!err);

    testThat(A.write(&err, "foo", 3) == 3);
    testThat(!err);

    char buff[4] = { '\0' };
    testThat(B.read(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "foo") == 0);

    testThat(B.write(&err, "bar", 3) == 3);
    testThat(!err);

    testThat(A.read(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "bar") == 0);
}

void hostName(void)
{
    nwl::ErrorCode err;
    auto s = nwl::SocketFactory::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(s.valid());
    nwl::String hostStr;
    nwl::SocketFactory::hostName(&err, &hostStr);
    testThat(!err);
    std::cout << hostStr << " ";
}

setupSuite(tsocketfactory_cpp)
{
    addTest(createTcpServer);
    addTest(createTcp6Server);
    addTest(connectAndTalkBlocking);
    addTest(endPoints);
    addTest(connectAndTalkMakeNonBlock);
    addTest(socketPair);
    addTest(hostName);
}
