/*! tsocket.cpp */

#include "defs.h"

#include <nwl/socket.hpp>

void invalidSocket(void)
{
    nwl::Socket s;
    nwl::ErrorCode err;
    char buff[3];

    testThat(!s.valid());
    testThat(s.native() == nwl::InvalidSocket);

    testThat(s.write(&err, "foo", 3) == 0);
    testThat(err);
    err.clear();

    testThat(s.read(&err, buff, 3) == 0);
    testThat(err);
    err.clear();

    s.shutDown(&err, nwl::ShutDownKind::Both);
    testThat(err);
    err.clear();

    s.close(&err);
    testThat(!err);

    s.setBlocking(&err, true);
    testThat(err);
    err.clear();

    s.setKeepAlive(&err, nwl::Second(1));
    testThat(err);
    err.clear();

    s.setNoDelay(&err, true);
    testThat(err);
    err.clear();

    s.setSendTimeout(&err, nwl::MilliSecond(300));
    testThat(err);
    err.clear();
}

setupSuite(tsocket_cpp)
{
    addTest(invalidSocket);
}
