/*! tloop.cpp */

#include "defs.h"

#include <nwl/loop.hpp>
#include <nwl/socketfactory.hpp>

#include <atomic>
#include <chrono>
#include <iostream>
#include <thread>
#include <vector>

using namespace nwl;

static std::atomic<int> numDone{ 0 };

struct ClientHandler : public SocketObserver
{
    int state = 0;
    char buff[4];
    SizeType read = 0;
    SizeType written = 0;

    void handleEvent(Loop* l,
                     const Socket& sock,
                     SocketEvent::Type events) override
    {
        buff[3] = '\0';
        int curr = state;
        if (curr == 0) {
            if (events & SocketEvent::Read) {
                ErrorCode err;
                read += sock.read(&err, buff + read, 3 - read);
                testThat(!err || SocketTraits::isBlockingError(err));
                if (read == 3) {
                    testThat(std::strcmp(buff, "foo") == 0);
                    state++;
                }
            }
        }
        else if (curr == 1) {
            if (events & SocketEvent::Write) {
                const char toWrite[] = "bar";
                ErrorCode err;
                written += sock.write(&err, toWrite + written, 3 - written);
                testThat(!err || SocketTraits::isBlockingError(err));
                if (written == 3) {
                    state++;
                }
            }
        }
        else {
            testThat(curr == 2);
            SocketToken id = sock.native();
            auto s = l->deregisterSocket(id);
            testThat(s.native() == id);
            ++numDone;
            delete this;
        }
    }
};

struct AcceptSocketObserver : public SocketObserver
{
    void handleEvent(Loop* l,
                     const Socket& sock,
                     SocketEvent::Type events) override
    {
        ErrorCode err;
        Endpoint ep;
        auto client = SocketFactory::accept(&err, &ep, sock);
        testThat(!err);
        testThat(client.valid());
        client.setBlocking(&err, false);
        testThat(!err);
        ClientHandler* ch = new ClientHandler;
        auto sid = l->registerSocket(
          std::move(client), ch, SocketEvent::Read | SocketEvent::Write);
        testThat(sid != InvalidSocketToken);
    }
};

void client(void)
{
    ErrorCode err;
    Socket c = SocketFactory::connect(&err, "127.0.0.1", 8080);
    testThat(c.valid());
    testThat(!err);
    char buff[] = "xxx";
    testThat(c.writeAll(&err, "foo", 3) == 3);
    testThat(!err);
    testThat(c.readAll(&err, buff, 3) == 3);
    testThat(!err);
    testThat(std::strcmp(buff, "bar") == 0);
    c.close(&err);
    testThat(!err);
}

void singleClient(void)
{
    ErrorCode err;
    Loop l;
    AcceptSocketObserver srvObserver;
    auto srv = SocketFactory::createTcpServer(&err, "127.0.0.1", 8080, 1);
    testThat(!err);
    testThat(srv.valid());
    srv.setBlocking(&err, false);
    testThat(!err);
    auto sid = l.registerSocket(
      std::move(srv), &srvObserver, SocketEvent::Read | SocketEvent::Write);
    testThat(sid != nwl::InvalidSocketToken);

    numDone = 0;
    std::thread cliThread(client);
    std::thread t([&l]() -> void {
        while (numDone != 1) {
            std::this_thread::sleep_for(std::chrono::microseconds(50));
        }
        l.stop();
    });

    l.run(&err);

    cliThread.join();
    t.join();

    testThat(!err);
}

void fiveClients(void)
{
    ErrorCode err;
    Loop l;
    AcceptSocketObserver srvListener;
    auto srv = SocketFactory::createTcpServer(&err, "127.0.0.1", 8080, 5);
    testThat(!err);
    testThat(srv.valid());
    srv.setBlocking(&err, false);
    testThat(!err);
    auto sid = l.registerSocket(
      std::move(srv), &srvListener, SocketEvent::Read | SocketEvent::Write);
    testThat(sid != nwl::InvalidSocketToken);

    std::vector<std::thread> clients;
    const int numClients = 5;
    numDone = 0;
    for (int i = 0; i < numClients; ++i) {
        clients.emplace_back(client);
    }
    std::thread t([&l, numClients]() -> void {
        while (numDone != numClients) {
            std::this_thread::sleep_for(std::chrono::microseconds(50));
        }
        l.stop();
    });

    l.run(&err);

    for (auto& cli : clients) {
        cli.join();
    }
    t.join();

    testThat(!err);
}

void timerEvents(void)
{
    struct TimerEventCb : public nwl::TimerObserver
    {
        std::vector<nwl::TimerToken>* vec;
        void handleEvent(nwl::Loop* loop, nwl::TimerToken id) override
        {
            vec->push_back(id);
            if (id == 2) {
                loop->stop();
            }
            else if (id == 0) {
                testThat(loop->deregisterTimeEvent(3));
            }
        }
    };

    std::vector<nwl::TimerToken> order;
    TimerEventCb a;
    a.vec = &order;
    TimerEventCb b;
    b.vec = &order;
    TimerEventCb c;
    c.vec = &order;
    TimerEventCb d;
    d.vec = &order;

    nwl::Loop l;
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(40), &a);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(10), &b);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(200), &c);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(250), &d);

    nwl::ErrorCode ec;
    l.run(&ec);

    testThat(order.size() == 3);
    testThat(order[0] == 1);
    testThat(order[1] == 0);
    testThat(order[2] == 2);

    order.clear();
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(40), &a);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(10), &b);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(200), &c);
    l.registerTimeEvent(nwl::TimerEvent::MilliSecond(250), &d);

    l.run(&ec);

    testThat(order.size() == 3);
    testThat(order[0] == 1);
    testThat(order[1] == 0);
    testThat(order[2] == 2);
}

setupSuite(tloop_cpp)
{
    addTest(singleClient);
    addTest(fiveClients);
    addTest(timerEvents);
}
