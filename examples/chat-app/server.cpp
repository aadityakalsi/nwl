/*! server.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <algorithm>
#include <deque>
#include <iostream>
#include <nwl/loop.hpp>
#include <nwl/socketfactory.hpp>
#include <vector>

using ChatPayload = std::string;

using ChatMessage = std::shared_ptr<ChatPayload>;

struct ClientAcceptor;

struct ChatUser : public nwl::SocketObserver
{
    nwl::Endpoint endpoint;
    std::string incoming;
    std::deque<ChatMessage> pending;
    std::size_t bytesWritten;
    ClientAcceptor* acceptor;

    ChatUser(nwl::Endpoint ep, ClientAcceptor* ca)
    : endpoint(ep)
    , incoming()
    , pending()
    , bytesWritten()
    , acceptor(ca)
    {
    }

    void postMessage(const nwl::Endpoint& ep, const std::string& msg);

    void handleEvent(nwl::Loop* loop,
                     const nwl::Socket& sock,
                     nwl::SocketEvent::Type events) override;
};

struct ClientAcceptor : public nwl::SocketObserver
{
    std::vector<std::unique_ptr<ChatUser>> users;

    void handleEvent(nwl::Loop* loop,
                     const nwl::Socket& sock,
                     nwl::SocketEvent::Type events) override
    {
        nwl::ErrorCode err;
        nwl::Endpoint ep;

        auto client = nwl::SocketFactory::accept(&err, &ep, sock);
        if (err)
            return;
        assert(client.valid());
        client.setBlocking(&err, false);
        assert(!err);

        users.emplace_back(new ChatUser(ep, this));
        ChatUser& newUser = *users.back();

        newUser.postMessage(newUser.endpoint,
                            "*** User has entered the building...\n");

        auto sid = loop->registerSocket(std::move(client),
                                        &newUser,
                                        nwl::SocketEvent::Read |
                                        nwl::SocketEvent::Write);
        assert(sid != nwl::InvalidSocketToken);
    }
};

ChatMessage makeMessage(const nwl::Endpoint& ep, const std::string& msg)
{
    return std::make_shared<ChatPayload>(
      ep.address + ":" + std::to_string(ep.portNum) + ": " + msg);
}

void ChatUser::postMessage(const nwl::Endpoint& ep, const std::string& msg)
{
    auto m = makeMessage(ep, msg);
    for (auto& user : acceptor->users) {
        if (user.get() != this) {
            user->pending.push_back(m);
        }
    }
}

void ChatUser::handleEvent(nwl::Loop* loop,
                           const nwl::Socket& sock,
                           nwl::SocketEvent::Type events)
{
    nwl::ErrorCode err;
    if (events & nwl::SocketEvent::Read) {
        char ch = 0;
        if (sock.read(&err, &ch, 1) != 1 && !nwl::isBlockingError(err)) {
            // socket closed, as ready to read but nothing read
            postMessage(endpoint, "*** User has left the building...\n");

            loop->deregisterSocket(sock.native());
            // cannot reference sock anymore

            // remove this user
            auto& users = acceptor->users;
            auto it = std::find_if(
              users.begin(),
              users.end(),
              [this](const std::unique_ptr<ChatUser>& up) -> bool {
                  return up.get() == this;
              });
            assert(it != users.end());
            users.erase(it);

            return;
        }
        else if (ch != 0 && !err) {
            incoming.push_back(ch);
            char buff[999];
            auto nRead = sock.read(&err, &buff, 999);
            incoming.append(buff, nRead);
            if (incoming.back() == '\n') {
                postMessage(endpoint, incoming);
                incoming.clear();
            }
        }
    }
    if (events & nwl::SocketEvent::Write) {
        if (pending.empty())
            return;
        auto& msg = pending.front();
        if (bytesWritten != msg->size()) {
            auto nWrite = sock.write(
              &err, msg->c_str() + bytesWritten, msg->size() - bytesWritten);
            bytesWritten += nWrite;
            if (nWrite == 0 && err && !nwl::isBlockingError(err)) {
                std::cerr << "Error writing to " << endpoint << ", "
                          << err.message() << "\n";
            }
            if (bytesWritten == msg->size()) {
                bytesWritten = 0;
                pending.pop_front();
            }
        }
    }
}

int main()
{
    nwl::Loop loop;
    ClientAcceptor acceptor;

    nwl::ErrorCode err;
    auto srv =
      nwl::SocketFactory::createTcpServer(&err, "0.0.0.0", 35813, 100);

    assert(srv.valid());
    assert(!err);

    loop.registerSocket(std::move(srv),
                        &acceptor,
                        nwl::SocketEvent::Read | nwl::SocketEvent::Write);

    loop.run(&err);
}
