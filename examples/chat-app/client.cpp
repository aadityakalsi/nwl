/*! client.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <deque>
#include <iostream>
#include <nwl/file.hpp>
#include <nwl/loop.hpp>
#include <nwl/socketfactory.hpp>
#include <thread>

#define DIE_ON(err)                                                    \
    if (err) {                                                         \
        std::cerr << '(' << __FILE__ << ":" << __LINE__ << ") " << err \
                  << std::endl;                                        \
        std::exit(-1);                                                 \
    }

struct Client : public nwl::SocketObserver
{
    std::deque<std::string> messages;
    std::string buffer;
    std::size_t bytesWritten;

    Client()
    : messages()
    , buffer()
    , bytesWritten()
    {
    }

    void handleEvent(nwl::Loop* loop,
                     const nwl::Socket& sock,
                     nwl::SocketEvent::Type events) override
    {
        nwl::ErrorCode err;
        if (events & nwl::SocketEvent::Read) {
            char ch = 0;
            if (sock.read(&err, &ch, 1) != 1 && !nwl::isBlockingError(err)) {
                std::cerr << "Server connection severed. Exiting...\n";
                std::exit(-1);
            }
            else if (!err) {
                buffer.push_back(ch);
                char buff[999];
                auto nRead = sock.read(&err, buff, 999);
                buffer.append(buff, nRead);
                if (buffer.back() == '\n') {
                    std::cout << buffer;
                    buffer.clear();
                }
            }
        }
        if (events & nwl::SocketEvent::Write) {
            if (messages.empty()) {
                return;
            }
            auto& msg = messages.front();
            if (bytesWritten != msg.size()) {
                auto nWrite = sock.write(&err,
                                         msg.c_str() + bytesWritten,
                                         msg.size() - bytesWritten);
                bytesWritten += nWrite;
                if (nWrite == 0 && err && !nwl::isBlockingError(err)) {
                    std::cerr << "Error writing to chat server, "
                              << err.message() << "\n";
                    std::exit(-1);
                }
                if (bytesWritten == msg.size()) {
                    bytesWritten = 0;
                    messages.pop_front();
                }
            }
        }
    }
};

struct StdinReader : public nwl::SocketObserver
{
    std::string buffer;
    Client* client;

    StdinReader(Client* cli)
    : buffer()
    , client(cli)
    {
    }

    void handleEvent(nwl::Loop* loop,
                     const nwl::Socket& sock,
                     nwl::SocketEvent::Type events) override
    {
        char buff[1000];
        nwl::ErrorCode err;
        auto nRead = sock.read(&err, buff, 1000);
        if (err && !nwl::isBlockingError(err)) {
            DIE_ON(err);
        }
        buffer.append(buff, nRead);
        if (!buffer.empty() && buffer.back() == '\n') {
            client->messages.push_back(buffer);
            buffer.clear();
        }
    }
};

int main()
{
    nwl::Loop loop;

    Client cli;
    StdinReader rdr(&cli);

    nwl::ErrorCode err;

    nwl::Socket inpSock;
    nwl::Socket outSock;
    nwl::SocketFactory::createSocketPair(&err, &inpSock, &outSock);
    DIE_ON(err);

    inpSock.setBlocking(&err, false);
    DIE_ON(err);
    outSock.setBlocking(&err, false);
    DIE_ON(err);

    std::thread inReader([&outSock]() -> void {
        std::string line;
        for (;;) {
            int data = ::getchar();
            if (data == EOF)
                continue;
            char ch = data;
            line.push_back(ch);
            if (ch == '\n') {
                nwl::ErrorCode err;
                outSock.writeAll(&err, line.c_str(), line.size());
                DIE_ON(err);
                line.clear();
            }
        }
    });

    auto srv = nwl::SocketFactory::connect(&err, "127.0.0.1", 35813);
    DIE_ON(err);
    srv.setBlocking(&err, false);
    DIE_ON(err);

    loop.registerSocket(
      std::move(srv), &cli, nwl::SocketEvent::Read | nwl::SocketEvent::Write);
    loop.registerSocket(std::move(inpSock), &rdr, nwl::SocketEvent::Read);

    std::cout.setf(std::ios::unitbuf);

    std::cout << "you: ";
    loop.run(&err);
    inReader.join();
}
