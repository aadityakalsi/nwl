/*! sockettraits.win.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_SOCKETTRAITS_WIN_HPP
#define NWL_SOCKETTRAITS_WIN_HPP

#include "nwl/log.hpp"
#include "nwl/platform.win.hpp"
#include <nwl/sockettraits.hpp>

#include <mutex>
#include <thread>

#undef EINPROGRESS
#undef EAGAIN
#undef EWOULDBLOCK
#undef EINTR
#define EINPROGRESS WSAEINPROGRESS
#define EAGAIN WSAEWOULDBLOCK
#define EWOULDBLOCK WSAEWOULDBLOCK
#define EINTR WSAEINTR

namespace nwl {

namespace {

class wsa_error_category : public std::error_category
{
    const char* name() const noexcept override { return "WinSock"; }

    String message(int err) const override
    {
        if (!err)
            return String();

        char msgbuf[256]; // for a message up to 255 bytes.
        msgbuf[0] = '\0'; // Microsoft doesn't guarantee this on man page.

        ::FormatMessage(
          FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
          NULL,                                      // lpsource
          err,                                       // message id
          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // languageid
          msgbuf,                                    // output buffer
          sizeof(msgbuf),                            // size of msgbuf, bytes
          NULL);                                     // va_list of arguments
        auto len = std::strlen(msgbuf);
        if (len > 2 && msgbuf[len - 1] == '\n' && msgbuf[len - 2] == '\r') {
            len -= 2;
        }
        return String(msgbuf, len);
    }
};

const wsa_error_category WSA_ERR_CAT{};

#define WINSOCK_ERR ErrorCode(WSAGetLastError(), WSA_ERR_CAT)

std::once_flag flag;

} // namespace

void SocketTraits::initSocketUsage()
{
    std::call_once(flag, []() -> void {
        ::WSADATA wsaData;
        auto err = ::WSAStartup(MAKEWORD(1, 1), &wsaData);
        assert(err == 0);
    });
}

void SocketTraits::close(ErrorCode* err, NativeSocket sock)
{
    if (!isValid(sock) || ::closesocket(sock) != SOCKET_ERROR) {
        *err = OK;
    }
    else {
        *err = WINSOCK_ERR;
    }
}

namespace {

SocketTraits::SizeType readInternal(ErrorCode* err,
                                    NativeSocket sock,
                                    void* data,
                                    SizeType size)
{
    assert(static_cast<int>(size) == size);
    auto nread = ::recv(sock, (char*)data, (int)size, 0);
    if (nread == SOCKET_ERROR) {
        int err_no = WSAGetLastError();
        if (err_no == EWOULDBLOCK || err_no == EAGAIN) {
            *err = ErrorCode(err_no, &WSA_ERR_CAT);
        }
        else {
            *err = WINSOCK_ERR;
        }
        nread = 0;
    }
    else {
        *err = OK;
    }
    return nread;
}

SocketTraits::SizeType writeInternal(ErrorCode* err,
                                     NativeSocket sock,
                                     const void* data,
                                     SizeType size)
{
    assert(static_cast<int>(size) == size);
    auto nwritten = ::send(sock, (const char*)data, (int)size, 0);
    if (nwritten == SOCKET_ERROR) {
        int err_no = WSAGetLastError();
        if (err_no == EWOULDBLOCK || err_no == EAGAIN) {
            *err = ErrorCode(err_no, &WSA_ERR_CAT);
        }
        else {
            *err = WINSOCK_ERR;
        }
        nwritten = 0;
    }
    else {
        *err = OK;
    }
    return nwritten;
}

} // namespace

SocketTraits::SizeType SocketTraits::read(ErrorCode* err,
                                          NativeSocket sock,
                                          void* data,
                                          SizeType size)
{
    if (size > (std::uint32_t)std::numeric_limits<int>::max()) {
        auto nRead =
          readInternal(err, sock, data, std::numeric_limits<int>::max());
        char* cdata = static_cast<char*>(data) + nRead;
        if (!*err) {
            return readInternal(err, sock, cdata, size - nRead);
        }
        else {
            return nRead;
        }
    }
    else {
        return readInternal(err, sock, data, size);
    }
}

SocketTraits::SizeType SocketTraits::write(ErrorCode* err,
                                           NativeSocket sock,
                                           const void* data,
                                           SizeType size)
{
    if (size > (std::uint32_t)std::numeric_limits<int>::max()) {
        auto nWritten =
          writeInternal(err, sock, data, std::numeric_limits<int>::max());
        const char* cdata = static_cast<const char*>(data) + nWritten;
        if (!*err) {
            return writeInternal(err, sock, cdata, size - nWritten);
        }
        else {
            return nWritten;
        }
    }
    else {
        return writeInternal(err, sock, data, size);
    }
}

void SocketTraits::shutDown(ErrorCode* err,
                            NativeSocket sock,
                            ShutDownKind::Type kind)
{
    if (::shutdown(sock, static_cast<int>(kind)) == -1) {
        *err = WINSOCK_ERR;
    }
    else {
        *err = OK;
    }
}

void SocketTraits::setBlocking(ErrorCode* err, NativeSocket sock, bool block)
{
    unsigned long val = block ? 0 : 1;
    if (::ioctlsocket(sock, FIONBIO, &val) == SOCKET_ERROR) {
        *err = WINSOCK_ERR;
    }
    else {
        *err = OK;
    }
}

void SocketTraits::setKeepAlive(ErrorCode* err,
                                NativeSocket sock,
                                Second kaTime)
{
    int val = 1;
    if (::setsockopt(
          sock, SOL_SOCKET, SO_KEEPALIVE, (const char*)&val, sizeof(val)) ==
        -1) {
        *err = WINSOCK_ERR;
        return;
    }
    (void)kaTime;
    *err = OK;
}

void SocketTraits::setNoDelay(ErrorCode* err, NativeSocket sock, bool noDelay)
{
    int val = noDelay ? 1 : 0;
    if (::setsockopt(
          sock, IPPROTO_TCP, TCP_NODELAY, (const char*)&val, sizeof(val)) ==
        -1) {
        *err = WINSOCK_ERR;
        return;
    }
    *err = OK;
}

void SocketTraits::setSendTimeout(ErrorCode* err,
                                  NativeSocket sock,
                                  MilliSecond timeout)
{
    ::timeval tv;
    tv.tv_sec = static_cast<long>(timeout) / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;
    if (::setsockopt(
          sock, SOL_SOCKET, SO_SNDTIMEO, (const char*)&tv, sizeof(tv)) ==
        -1) {
        *err = WINSOCK_ERR;
        return;
    }
    *err = OK;
}

ErrorCode SocketTraits::lastError()
{
    return WINSOCK_ERR;
}

bool SocketTraits::isBlockingError(const ErrorCode& err)
{
    if (&err.category() != &WSA_ERR_CAT)
        return false;
    switch (err.value()) {
    case WSAEINPROGRESS:
    case WSAEWOULDBLOCK:
    case WSAEINTR:
        return true;
    default:
        return false;
    }
}

} // namespace nwl

#endif /*NWL_SOCKETTRAITS_WIN_HPP*/
