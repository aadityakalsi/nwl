/*! filefactory.win.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILEFACTORY_WIN_HPP
#define NWL_FILEFACTORY_WIN_HPP

#include <nwl/file.hpp>
#include <nwl/filefactory.hpp>

#include "nwl/log.hpp"
#include "nwl/platform.win.hpp"

#define WIN_ERR ErrorCode(::GetLastError(), WIN_ERR_CAT)

namespace nwl {

namespace {

struct win_err_category : public std::error_category
{
    const char* name() const noexcept override { return "WinAPI"; }

    String message(int err) const override
    {
        if (!err)
            return String();

        char msgbuf[256]; // for a message up to 255 bytes.
        msgbuf[0] = '\0'; // Microsoft doesn't guarantee this on man page.

        ::FormatMessage(
          FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
          NULL,                                      // lpsource
          err,                                       // message id
          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // languageid
          msgbuf,                                    // output buffer
          sizeof(msgbuf),                            // size of msgbuf, bytes
          NULL);                                     // va_list of arguments
        auto len = std::strlen(msgbuf);
        if (len > 2 && msgbuf[len - 1] == '\n' && msgbuf[len - 2] == '\r') {
            len -= 2;
        }
        return String(msgbuf, len);
    }
};

const win_err_category WIN_ERR_CAT{};

void flags(int* flags,
           int* advice,
           FileMode::Type mode,
           FileUsage::Type intent)
{
    switch (mode) {
    case FileMode::Read:
        *flags = GENERIC_READ;
        break;
    case FileMode::Append:
    case FileMode::Both:
        *flags = GENERIC_READ | GENERIC_WRITE;
        break;
    default:
        *flags = GENERIC_WRITE;
        break;
    }
    switch (intent) {
    case FileUsage::Sequential:
        *advice = mode == FileMode::Read ? FILE_FLAG_SEQUENTIAL_SCAN
                                         : FILE_FLAG_RANDOM_ACCESS;
        break;
    case FileUsage::Random:
        *advice = FILE_FLAG_RANDOM_ACCESS;
        break;
    }
}

} // namespace

File FileFactory::open(ErrorCode* err,
                       const String& file,
                       FileMode::Type mode,
                       FileUsage::Type intent)
{
    NativeFile fd = InvalidFile;
    int oflags = 0;
    int advice = 0;
    flags(&oflags, &advice, mode, intent);
    fd = ::CreateFileA(
      file.c_str(), oflags, 0, nullptr, OPEN_EXISTING, advice, nullptr);
    if (fd == InvalidFile) {
        *err = WIN_ERR;
    }
    else {
        *err = OK;
    }
    return File(fd);
}

File FileFactory::create(ErrorCode* err,
                         const String& file,
                         FileMode::Type mode,
                         FileUsage::Type intent)
{
    NativeFile fd = InvalidFile;
    int oflags = 0;
    int advice = 0;
    flags(&oflags, &advice, mode, intent);
    fd = ::CreateFileA(
      file.c_str(), oflags, 0, nullptr, CREATE_NEW, advice, nullptr);
    if (fd == InvalidFile) {
        *err = WIN_ERR;
    }
    else {
        *err = OK;
    }
    return File(fd);
}

void FileFactory::erase(ErrorCode* err, const String& file)
{
    *err = ::DeleteFileA(file.c_str()) ? OK : WIN_ERR;
}

FileFactory::SizeType FileFactory::size(ErrorCode* err, const String& file)
{
    ::WIN32_FILE_ATTRIBUTE_DATA data;
    ::LARGE_INTEGER li;
    if (::GetFileAttributesEx(file.c_str(), GetFileExInfoStandard, &data)) {
        *err = OK;
        li.LowPart = data.nFileSizeLow;
        li.HighPart = data.nFileSizeHigh;
    }
    else {
        *err = WIN_ERR;
    }
    return li.QuadPart;
}

} // namespace nwl

#endif /*NWL_FILEFACTORY_WIN_HPP*/
