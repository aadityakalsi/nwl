
/*! threadpool.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nwl/threadpool.hpp>

#include "nwl/log.hpp"

#include <algorithm>
#include <atomic>
#include <list>
#include <mutex>
#include <thread>
#include <vector>

namespace nwl {

struct ThreadPool::Impl
{
    struct Thread
    {
        std::mutex d_mutex;
        std::list<Job> d_jobs;
        std::atomic<bool> d_quit;
        std::thread d_thread;

        Thread()
        : d_quit(false)
        , d_thread()
        {
        }

        void start()
        {
            std::thread t([this]() -> void { this->body(); });
            d_thread = std::move(t);
        }

        void body()
        {
            for (;;) {
                try {
                    if (shouldQuit()) {
                        drainQueue();
                        break;
                    }
                    popAndRun();
                } catch (const std::exception& e) {
                    LOG << "Caught exception: " << e.what() << "\n";
                } catch (...) {
                    LOG << "Caught unknown non-exception\n";
                }
            }
        }

        bool shouldQuit() const
        {
            return d_quit.load(std::memory_order_acquire);
        }

        void startQuit() { d_quit.store(true, std::memory_order_release); }

        void popAndRun()
        {
            std::list<Job> j;
            if (tryPop(j)) {
                Job x = std::move(j.front());
                j.pop_front();
                x();
                assert(j.empty());
            }
        }

        bool tryPop(std::list<Job>& j)
        {
            if (d_mutex.try_lock()) {
                if (!d_jobs.empty()) {
                    assert(d_jobs.begin() != d_jobs.end());
                    j.splice(j.begin(), d_jobs, d_jobs.begin());
                }
                d_mutex.unlock();
                return !j.empty();
            }
            else {
                return false;
            }
        }

        void drainQueue()
        {
            assert(shouldQuit());
            // no need for locks
            // cannot enqueue any more tasks
            while (1) {
                bool isEmpty;
                std::list<Job> j;
                {
                    std::unique_lock<std::mutex> lock(d_mutex);
                    isEmpty = d_jobs.empty();
                    if (!isEmpty) {
                        assert(d_jobs.begin() != d_jobs.end());
                        j.splice(j.begin(), d_jobs, d_jobs.begin());
                    }
                }
                if (isEmpty) {
                    break;
                }
                else {
                    Job x = std::move(j.front());
                    j.pop_front();
                    assert(j.empty());
                    x();
                }
            }
        }

        bool push(Job&& j)
        {
            if (shouldQuit()) {
                return false;
            }
            bool done = true;
            {
                std::unique_lock<std::mutex> lock(d_mutex);
                try {
                    d_jobs.emplace_back(std::forward<Job>(j));
                } catch (...) {
                    done = false;
                }
            }
            return done;
        }

        void join() { d_thread.join(); }
    };

    std::vector<std::unique_ptr<Thread>> d_threads;
    std::atomic<size_t> d_idx;

    ~Impl()
    {
        for (auto& thread : d_threads) {
            thread->startQuit();
        }
        for (auto& thread : d_threads) {
            thread->join();
        }
    }

    void init(size_t numThreads)
    {
        d_idx = 0;
        d_threads.reserve(numThreads);
        for (size_t i = 0; i < numThreads; ++i) {
            d_threads.emplace_back(new Thread);
        }
        for (auto& thread : d_threads) {
            thread->start();
        }
    }

    bool enqueue(Job&& j)
    {
        size_t tidx = d_idx++;
        tidx %= d_threads.size();
        return d_threads[tidx]->push(std::forward<Job>(j));
    }
};

ThreadPool::ThreadPool()
: d_impl(new Impl)
{
    d_impl->init(std::thread::hardware_concurrency());
}

ThreadPool::ThreadPool(size_t numThreads)
: d_impl(new Impl)
{
    d_impl->init(numThreads);
}

ThreadPool::~ThreadPool() {}

bool ThreadPool::enqueue(ThreadPool::Job j)
{
    return d_impl->enqueue(std::move(j));
}

} // namespace nwl
