/*! filetraits.unix.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILETRAITS_UNIX_HPP
#define NWL_FILETRAITS_UNIX_HPP

#include <limits>

#include "nwl/platform.unix.hpp"

#include <nwl/filetraits.hpp>

#include "nwl/log.hpp"

namespace nwl {

void FileTraits::close(ErrorCode* err, NativeFile fd)
{
    if (!isValid(fd) || ::close(fd) != -1) {
        *err = OK;
    }
    else {
        *err = ERR;
    }
}

FileTraits::SizeType FileTraits::read(ErrorCode* err,
                                      NativeFile fd,
                                      void* data,
                                      SizeType size)
{
    auto nread = ::read(fd, data, size);
    if (nread == -1) {
        int err_no = errno;
        if (err_no == EAGAIN) {
            *err = OK;
        }
        else {
            *err = ERR;
        }
        nread = 0;
    }
    else {
        *err = OK;
    }
    return nread;
}

FileTraits::SizeType FileTraits::write(ErrorCode* err,
                                       NativeFile fd,
                                       const void* data,
                                       SizeType size)
{
    auto nwritten = ::write(fd, data, size);
    if (nwritten == -1) {
        int err_no = errno;
        if (err_no == EAGAIN) {
            *err = OK;
        }
        else {
            *err = ERR;
        }
        nwritten = 0;
    }
    else {
        *err = OK;
    }
    return nwritten;
}

FileTraits::OffsetType FileTraits::tell(ErrorCode* err, NativeFile fd)
{
    auto pos = ::lseek(fd, 0, SEEK_CUR);
    if (pos == static_cast<decltype(pos)>(-1)) {
        *err = ERR;
        return 0;
    }
    else {
        *err = OK;
        return pos;
    }
}

void FileTraits::seek(ErrorCode* err,
                      NativeFile fd,
                      FileTraits::OffsetType off)
{
    auto pos = ::lseek(fd, off, SEEK_SET);
    if (pos == static_cast<decltype(pos)>(-1)) {
        *err = ERR;
    }
    else {
        *err = OK;
    }
}

} // namespace nwl

#endif /*NWL_FILETRAITS_UNIX_HPP*/
