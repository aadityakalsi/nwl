/*! platform.unix.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_PLATFORM_UNIX_HPP
#define NWL_PLATFORM_UNIX_HPP

#if !defined(__unix__) && !defined(UNIX) && !defined(__APPLE__)
#    error "Incorrect platform to include file"
#endif

#ifndef _BSD_SOURCE
#    define _BSD_SOURCE
#endif

#ifdef __linux__
#    ifndef _GNU_SOURCE
#        define _GNU_SOURCE
#    endif
#    ifndef _XOPEN_SOURCE
#        define _XOPEN_SOURCE 700
#    endif
#    ifndef _LARGEFILE_SOURCE
#        define _LARGEFILE_SOURCE
#    endif
#    ifndef _FILE_OFFSET_BITS
#        define _FILE_OFFSET_BITS 64
#    endif
#endif

#include <fcntl.h>
#include <sys/socket.h>
#include <unistd.h>

#include <netinet/in.h>
#include <netinet/tcp.h>

#include <fcntl.h>
#include <netdb.h>
#include <sys/ioctl.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <unistd.h>

#include <arpa/inet.h>
#include <net/if.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

#include <signal.h>

#endif /*NWL_PLATFORM_UNIX_HPP*/
