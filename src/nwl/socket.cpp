/*! socket.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nwl/socket.hpp>

#ifdef _WIN32
#    include "nwl/platform.win.hpp"
#else
#    include "nwl/platform.unix.hpp"
#endif

#include "nwl/log.hpp"

namespace nwl {

Socket::Socket() noexcept
: d_fd(InvalidSocket)
{
}

Socket::Socket(NativeSocket fd) noexcept
: d_fd(fd)
{
    // LOG << "New socket: " << fd << "\n";
}

Socket::~Socket() noexcept
{
    ErrorCode err;
    this->close(&err);
    // if (err) {
    //     LOG << "closing socket: " << fd << "; err: " << err.message() <<
    //     "\n";
    // }
}

Socket::Socket(Socket&& rhs) noexcept
: d_fd(rhs.d_fd)
{
    rhs.d_fd = InvalidSocket;
}

Socket& Socket::operator=(Socket&& rhs) noexcept
{
    if (this != &rhs) {
        ErrorCode err;
        close(&err);
        d_fd = rhs.d_fd;
        rhs.d_fd = InvalidSocket;
    }
    return *this;
}

// ACCESSORS
bool Socket::valid() const
{
    return SocketTraits::isValid(d_fd);
}

NativeSocket Socket::native() const
{
    return d_fd;
}

Socket::SizeType Socket::write(ErrorCode* err,
                               const void* data,
                               SizeType count) const
{
    return SocketTraits::write(err, d_fd, data, count);
}

Socket::SizeType Socket::read(ErrorCode* err,
                              void* data,
                              SizeType count) const
{
    return SocketTraits::read(err, d_fd, data, count);
}

Socket::SizeType Socket::writeAll(ErrorCode* err,
                                  const void* data,
                                  SizeType count) const
{
    const char* strdata = static_cast<const char*>(data);
    err->clear();
    while ((!(*err) || SocketTraits::isBlockingError(*err)) && count > 0) {
        auto wr = write(err, strdata, count);
        strdata += wr;
        count -= wr;
    }
    return static_cast<Socket::SizeType>(strdata - (const char*)data);
}

Socket::SizeType Socket::readAll(ErrorCode* err,
                                 void* data,
                                 SizeType count) const
{
    char* strdata = static_cast<char*>(data);
    err->clear();
    while ((!(*err) || SocketTraits::isBlockingError(*err)) && count > 0) {
        auto rd = read(err, strdata, count);
        strdata += rd;
        count -= rd;
    }
    return static_cast<Socket::SizeType>(strdata - (char*)data);
}

void Socket::shutDown(ErrorCode* err, ShutDownKind::Type kind) const
{
    SocketTraits::shutDown(err, d_fd, kind);
}

void Socket::close(ErrorCode* err)
{
    SocketTraits::close(err, d_fd);
    d_fd = InvalidSocket;
}

void Socket::setBlocking(ErrorCode* err, bool on) const
{
    SocketTraits::setBlocking(err, d_fd, on);
}

void Socket::setKeepAlive(ErrorCode* err, Second kaTime) const
{
    SocketTraits::setKeepAlive(err, d_fd, kaTime);
}

void Socket::setNoDelay(ErrorCode* err, bool on) const
{
    SocketTraits::setNoDelay(err, d_fd, on);
}

void Socket::setSendTimeout(ErrorCode* err, MilliSecond timeout) const
{
    SocketTraits::setSendTimeout(err, d_fd, timeout);
}

} // namespace nwl
