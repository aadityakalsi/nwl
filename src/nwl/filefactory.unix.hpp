/*! filefactory.unix.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILEFACTORY_UNIX_HPP
#define NWL_FILEFACTORY_UNIX_HPP

#include <nwl/filefactory.hpp>

#include "nwl/log.hpp"
#include "nwl/platform.unix.hpp"

namespace nwl {

namespace {

#ifdef __APPLE__
#    define POSIX_FADV_SEQUENTIAL 0
#    define POSIX_FADV_NORMAL 0
#    define POSIX_FADV_RANDOM 0
#endif

void flags(int* flags,
           int* advice,
           FileMode::Type mode,
           FileUsage::Type intent)
{
    switch (mode) {
    case FileMode::Read:
        *flags = O_RDONLY;
        break;
    case FileMode::Append:
        *flags = O_RDWR | O_APPEND;
        break;
    case FileMode::Write:
        *flags = O_WRONLY;
        break;
    default:
        *flags = O_RDWR;
        break;
    }
    switch (intent) {
    case FileUsage::Sequential:
        *advice =
          mode == FileMode::Read ? POSIX_FADV_SEQUENTIAL : POSIX_FADV_NORMAL;
        break;
    case FileUsage::Random:
        *advice = POSIX_FADV_RANDOM;
        break;
    }
}

} // namespace

File FileFactory::open(ErrorCode* err,
                       const String& file,
                       FileMode::Type mode,
                       FileUsage::Type intent)
{
    NativeFile fd = InvalidFile;
    int oflags = 0;
    int advice = 0;
    flags(&oflags, &advice, mode, intent);
    fd = ::open(file.c_str(), oflags);
    if (fd == InvalidFile) {
        *err = ERR;
    }
    else {
        *err = OK;
#ifndef __APPLE__
        if (::posix_fadvise(fd, 0, 0, advice) != 0) {
            *err = ERR;
        }
#endif
    }
    return File(fd);
}

File FileFactory::create(ErrorCode* err,
                         const String& file,
                         FileMode::Type mode,
                         FileUsage::Type intent)
{
    NativeFile fd = InvalidFile;
    int oflags = 0;
    int advice = 0;
    flags(&oflags, &advice, mode, intent);
    fd = ::open(file.c_str(), oflags | O_CREAT | O_EXCL, 0644);
    if (fd == InvalidFile) {
        *err = ERR;
    }
    else {
        *err = OK;
#ifndef __APPLE__
        if (::posix_fadvise(fd, 0, 0, advice) != 0) {
            *err = ERR;
        }
#endif
    }
    return File(fd);
}

void FileFactory::erase(ErrorCode* err, const String& file)
{
    *err = ::unlink(file.c_str()) ? ERR : OK;
}

FileFactory::SizeType FileFactory::size(ErrorCode* err, const String& file)
{
    struct ::stat s;
    SizeType fileSize = 0;
    if (::stat(file.c_str(), &s) != 0) {
        *err = ERR;
    }
    else if (S_ISREG(s.st_mode)) {
        *err = OK;
        fileSize = s.st_size;
    }
    else {
        *err = ErrorCode(ENOENT, std::generic_category());
    }
    return fileSize;
}

} // namespace nwl

#endif /*NWL_FILEFACTORY_UNIX_HPP*/
