/*! filetraits.win.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_FILETRAITS_WIN_HPP
#define NWL_FILETRAITS_WIN_HPP

#include "nwl/log.hpp"
#include "nwl/platform.win.hpp"
#include <nwl/types.hpp>

#include <type_traits>

#define WIN_ERR ErrorCode(::GetLastError(), WIN_ERR_CAT)

namespace nwl {

namespace {

struct win_err_category : public std::error_category
{
    const char* name() const noexcept override { return "WinAPI"; }

    String message(int err) const override
    {
        if (!err)
            return String();

        char msgbuf[256]; // for a message up to 255 bytes.
        msgbuf[0] = '\0'; // Microsoft doesn't guarantee this on man page.

        ::FormatMessage(
          FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
          NULL,                                      // lpsource
          err,                                       // message id
          MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), // languageid
          msgbuf,                                    // output buffer
          sizeof(msgbuf),                            // size of msgbuf, bytes
          NULL);                                     // va_list of arguments
        auto len = std::strlen(msgbuf);
        if (len > 2 && msgbuf[len - 1] == '\n' && msgbuf[len - 2] == '\r') {
            len -= 2;
        }
        return String(msgbuf, len);
    }
};

const win_err_category WIN_ERR_CAT{};

} // namespace

void FileTraits::close(ErrorCode* err, NativeFile fd)
{
    if (!isValid(fd) || ::CloseHandle(fd)) {
        *err = OK;
    }
    else {
        *err = WIN_ERR;
    }
}

static_assert(sizeof(DWORD) == 4, "DWORD must be 4 bytes");
static_assert(std::is_unsigned<DWORD>::value, "DWORD must be unsigned");

FileTraits::SizeType FileTraits::read(ErrorCode* err,
                                      NativeFile fd,
                                      void* buff,
                                      SizeType size)
{
    assert(size <= std::numeric_limits<DWORD>::max());
    DWORD bytesRead;
    if (::ReadFile(fd, buff, (DWORD)size, &bytesRead, nullptr)) {
        *err = OK;
    }
    else {
        *err = WIN_ERR;
    }
    return static_cast<SizeType>(bytesRead);
}

FileTraits::SizeType FileTraits::write(ErrorCode* err,
                                       NativeFile fd,
                                       const void* buff,
                                       SizeType size)
{
    assert(size <= std::numeric_limits<DWORD>::max());
    DWORD bytesWritten;
    if (::WriteFile(fd, buff, (DWORD)size, &bytesWritten, nullptr)) {
        *err = OK;
    }
    else {
        *err = WIN_ERR;
    }
    return static_cast<SizeType>(bytesWritten);
}

FileTraits::OffsetType FileTraits::tell(ErrorCode* err, NativeFile fd)
{
    ::LARGE_INTEGER toMove;
    ::LARGE_INTEGER currPos;

    toMove.QuadPart = 0;
    if (::SetFilePointerEx(fd, toMove, &currPos, FILE_CURRENT)) {
        *err = OK;
    }
    else {
        *err = WIN_ERR;
    }
    return static_cast<OffsetType>(currPos.QuadPart);
}

void FileTraits::seek(ErrorCode* err,
                      NativeFile fd,
                      FileTraits::OffsetType off)
{
    ::LARGE_INTEGER toMove;
    toMove.QuadPart = off;
    if (::SetFilePointerEx(fd, toMove, nullptr, FILE_BEGIN)) {
        *err = OK;
    }
    else {
        *err = WIN_ERR;
    }
}

} // namespace nwl

#endif /*NWL_FILETRAITS_WIN_HPP*/
