/*! socketfactory.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifdef _WIN32
#    include "nwl/ext/socketpair.h"
#    include "nwl/platform.win.hpp"
#else
#    include "nwl/platform.unix.hpp"
#endif

#include <nwl/socketfactory.hpp>

#include <csignal>
#include <vector>

#include "nwl/log.hpp"

namespace nwl {

namespace {

static const int InitSocketTraits = ([]() -> int {
    SocketTraits::initSocketUsage();
    return 1;
})();

struct gai_error_category : public std::error_category
{
    const char* name() const noexcept override { return "getaddrinfo"; }

    String message(int err) const override { return ::gai_strerror(err); }
};

const gai_error_category GAI_ERR_CAT{};

struct AddrInfoDeleter
{
    addrinfo* info;

    AddrInfoDeleter(addrinfo* inf)
    : info(inf)
    {
    }
    ~AddrInfoDeleter()
    {
        if (info) {
            ::freeaddrinfo(info);
        }
    }
};

bool makeV6Only(ErrorCode* err, NativeSocket sock)
{
    int val = 1;
    if (::setsockopt(
          sock, IPPROTO_TCP, IPV6_V6ONLY, (const char*)&val, sizeof(val)) ==
        -1) {
        *err = SocketTraits::lastError();
        return false;
    }
    else {
        *err = OK;
        return true;
    }
}

bool setReuseAddr(ErrorCode* err, NativeSocket sock)
{
    int val = 1;
    if (::setsockopt(
          sock, SOL_SOCKET, SO_REUSEADDR, (const char*)&val, sizeof(val)) ==
        -1) {
        *err = SocketTraits::lastError();
        return false;
    }
#ifdef SO_REUSEPORT
    if (::setsockopt(sock, SOL_SOCKET, SO_REUSEPORT, &val, sizeof(val)) ==
        -1) {
        *err = SocketTraits::lastError();
        return false;
    }
#endif
    *err = OK;
    return true;
}

bool tryListen(ErrorCode* err,
               NativeSocket sock,
               ::sockaddr* sa,
               ::socklen_t len,
               int backlog)
{
    if (::bind(sock, sa, len) == -1) {
        *err = SocketTraits::lastError();
        return false;
    }
    if (::listen(sock, backlog) == -1) {
        *err = SocketTraits::lastError();
        return false;
    }
    *err = OK;
    return true;
}

NativeSocket createServer(ErrorCode* err,
                          const String& host,
                          Port p,
                          int backlog,
                          int af)
{
    String portstr = std::to_string(p);
    ::addrinfo hints, *sinfo;
    NativeSocket sock = InvalidSocket;

    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family = af;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = AI_PASSIVE;

    const int rv =
      ::getaddrinfo(host.c_str(), portstr.c_str(), &hints, &sinfo);
    if (rv != 0) {
        *err = ErrorCode(rv, GAI_ERR_CAT);
    }

    AddrInfoDeleter aid(sinfo);
    for (addrinfo* p = sinfo; p; p = p->ai_next) {
        sock = ::socket(p->ai_family, p->ai_socktype, p->ai_protocol);
        if (sock == -1) {
            continue;
        }
        if ((af == AF_INET6 && makeV6Only(err, sock) == false) ||
            setReuseAddr(err, sock) == false ||
            tryListen(
              err, sock, p->ai_addr, (::socklen_t)p->ai_addrlen, backlog) ==
              false) {
            ErrorCode err;
            SocketTraits::close(&err, sock);
            return InvalidSocket;
        }
        else {
            *err = OK;
            return sock;
        }
    }
    *err = ErrorCode(SocketCreationFailure, NWL_ERR_CAT);
    return InvalidSocket;
}

const int IP_LEN = 50;

void assignSockaddrToEndpoint(Endpoint* ep, ::sockaddr_storage& sa)
{
    ep->address.resize(IP_LEN);
    if (sa.ss_family == AF_INET) {
        ::sockaddr_in* s = (::sockaddr_in*)&sa;
        ::inet_ntop(AF_INET, (void*)&s->sin_addr, &ep->address[0], IP_LEN);
        ep->portNum = ntohs(s->sin_port);
    }
    else {
        ::sockaddr_in6* s = (::sockaddr_in6*)&sa;
        ::inet_ntop(AF_INET6, (void*)&s->sin6_addr, &ep->address[0], IP_LEN);
        ep->portNum = ntohs(s->sin6_port);
    }
    ep->address.resize(std::strlen(ep->address.c_str()));
}

} // namespace

#if 0

void SocketFactory::hostName(ErrorCode* err, String* host, String* numeric)
{
    char hostname[1024];
    hostname[1023] = '\0';
#    if defined(_WIN32)
    if (!::GetComputerNameEx(ComputerNameDnsFullyQualified, hostname, 1023)) {
        std::strcpy(hostname, "0.0.0.0");
    }
#    else
    ::gethostname(hostname, 1023);
#    endif

    bool done = false;
    String numericAddr;
#    if defined(_WIN32)

#    else
    ::ifconf ifc;
    Socket sock(::socket(AF_INET, SOCK_DGRAM, 0));
    assert(sock.valid());

    ifc.ifc_len = 0;
    ifc.ifc_req = 0;

    std::vector<char> _data;
    int rv = 0;
    std::size_t sz = 512;
#        if defined(__APPLE__)
    do {
        sz = sz << 1;
        _data.resize(sz);
        ifc.ifc_len = sz;
        ifc.ifc_buf = &_data[0];
        rv = ::ioctl(sock.native(), SIOCGIFCONF, &ifc);
    } while (rv >= 0 && ifc.ifc_len >= sz);
#        else
    rv = ::ioctl(sock.native(), SIOCGIFCONF, &ifc);
    if (rv >= 0) {
        sz = ifc.ifc_len;
        _data.resize(sz);
        ifc.ifc_buf = &_data[0];
    }
#        endif

    if (rv < 0 || ::ioctl(sock.native(), SIOCGIFCONF, &ifc) != 0) {
        *err = ErrorCode(EFAULT, std::generic_category());
        return;
    }

    _data.resize(sz);
    char* curr = &_data[0];
    char* const end = &_data[sz];
    while (curr < end) {
        auto ifr = reinterpret_cast<::ifreq*>(curr);
        if (ifr->ifr_addr.sa_family == AF_INET) {
            char _host[NI_MAXHOST] = {'\0'};
            if (::getnameinfo(&ifr->ifr_addr, sizeof(::sockaddr_in), _host, sizeof(_host), 0, 0, NI_NUMERICHOST) == 0 && std::strcmp(_host, "127.0.0.1") != 0) {
                auto sa = reinterpret_cast<::sockaddr_in*>(&ifr->ifr_addr);
                auto ia = reinterpret_cast<::in_addr*>(&sa->sin_addr.s_addr);
                numericAddr = ::inet_ntoa(*ia);
                done = true;
                break;
            }
        }

#        if defined(__APPLE__)
        curr += sizeof(ifr->ifr_name) + std::max((std::size_t)ifr->ifr_addr.sa_len, sizeof(ifr->ifr_addr));
#        else
        curr += sizeof(::ifreq);
#        endif
    }

    if (!done) {
        *err = ErrorCode(EFAULT, std::generic_category());
    }
#    endif

    if (!done) {
        assert(*err);
        return;
    }

    *host = hostname;
    *numeric = numericAddr;
}

#endif

void SocketFactory::hostName(ErrorCode* err, String* host)
{
    char hostname[1024];
    hostname[1023] = '\0';
#if defined(_WIN32)
    if (!::GetComputerNameEx(ComputerNameDnsFullyQualified, hostname, 1023)) {
        std::strcpy(hostname, "0.0.0.0");
    }
#else
    ::gethostname(hostname, 1023);
#endif

    *err = OK;
    *host = hostname;
}

Socket SocketFactory::createTcpServer(ErrorCode* err,
                                      const String& host,
                                      Port p,
                                      int backlog)
{
    return Socket(createServer(err, host, p, backlog, AF_INET));
}

Socket SocketFactory::createTcp6Server(ErrorCode* err,
                                       const String& host,
                                       Port p,
                                       int backlog)
{
    return Socket(createServer(err, host, p, backlog, AF_INET6));
}

Socket SocketFactory::accept(ErrorCode* err,
                             Endpoint* from,
                             const Socket& lstnSock)
{
    NativeSocket s = InvalidSocket;
    ::sockaddr_storage sa;
    ::socklen_t slen = sizeof(sa);
    s = ::accept(lstnSock.native(), (::sockaddr*)&sa, &slen);
    if (s == InvalidSocket) {
        auto errorCode = SocketTraits::lastError();
        if (SocketTraits::isBlockingError(errorCode)) {
            *err = OK;
        }
        else {
            *err = errorCode;
        }
        return Socket();
    }
    else {
        assignSockaddrToEndpoint(from, sa);
        return Socket(s);
    }
}

Socket SocketFactory::connectOpts(ErrorCode* err,
                                  const String& host,
                                  Port p,
                                  const String& bindAddr,
                                  bool nonBlock)
{
    NativeSocket s = InvalidSocket;
    String portstr = std::to_string(p);
    ::addrinfo hints, *sinfo;

    std::memset(&hints, 0, sizeof(hints));
    hints.ai_family = AF_UNSPEC;
    hints.ai_socktype = SOCK_STREAM;

    *err = OK;

    const int rv =
      ::getaddrinfo(host.c_str(), portstr.c_str(), &hints, &sinfo);
    if (rv != 0) {
        *err = ErrorCode(rv, GAI_ERR_CAT);
        std::cerr << *err << std::endl;
        return Socket();
    }
    AddrInfoDeleter aid(sinfo);
    for (::addrinfo* pPtr = sinfo; pPtr; pPtr = pPtr->ai_next) {
        s = ::socket(pPtr->ai_family, pPtr->ai_socktype, pPtr->ai_protocol);
        if (s == InvalidSocket) {
            continue;
        }
        Socket sock(s);
        if (setReuseAddr(err, s) == false) {
            return Socket();
        }
        if (!bindAddr.empty()) {
            ::addrinfo* binfo;
            const int rv =
              ::getaddrinfo(bindAddr.c_str(), nullptr, &hints, &binfo);
            if (rv != 0) {
                *err = ErrorCode(rv, GAI_ERR_CAT);
                return Socket();
            }
            AddrInfoDeleter del(binfo);
            bool done = false;
            for (::addrinfo* b = binfo; !done && b; b = b->ai_next) {
                if (::bind(sock.native(),
                           b->ai_addr,
                           (::socklen_t)b->ai_addrlen) == 0) {
                    done = true;
                }
            }
            if (!done) {
                *err = SocketTraits::lastError();
                return Socket();
            }
        }
        if (nonBlock) {
            sock.setBlocking(err, false);
            if (*err) {
                return Socket();
            }
        }
        if (::connect(sock.native(),
                      pPtr->ai_addr,
                      (::socklen_t)pPtr->ai_addrlen) == InvalidSocket) {
            auto errorCode = SocketTraits::lastError();
            if (SocketTraits::isBlockingError(errorCode) && nonBlock) {
                return sock;
            }
            else {
                *err = errorCode;
                return Socket();
            }
        }
        else {
            return sock;
        }
    }
    *err = ErrorCode(SocketCreationFailure, NWL_ERR_CAT);
    return Socket();
}

Socket SocketFactory::connect(ErrorCode* err, const String& host, Port p)
{
    return connectOpts(err, host, p, String(), false);
}

Socket SocketFactory::connectNonBlocking(ErrorCode* err,
                                         const String& host,
                                         Port p)
{
    return connectOpts(err, host, p, String(), true);
}

Socket SocketFactory::connectNonBlockingBind(ErrorCode* err,
                                             const String& host,
                                             Port p,
                                             const String& bindAddr)
{
    return connectOpts(err, host, p, bindAddr, true);
}

void SocketFactory::createSocketPair(ErrorCode* err,
                                     Socket* sockA,
                                     Socket* sockB)
{
#ifdef _WIN32
    NativeSocket socks[2];
    if (::dumb_socketpair((SOCKET*)socks, 0) == 0) {
        *err = OK;
        *sockA = Socket(socks[0]);
        *sockB = Socket(socks[1]);
    }
    else {
        *err = SocketTraits::lastError();
    }
#else
    int fd[2];
    int rv = ::socketpair(AF_LOCAL, SOCK_STREAM, 0, fd);
    if (rv != 0) {
        *err = ERR;
    }
    else {
        *err = OK;
        *sockA = Socket(fd[0]);
        *sockB = Socket(fd[1]);
    }
#endif
}

void SocketFactory::peerEndpoint(ErrorCode* err,
                                 Endpoint* ep,
                                 const Socket& peer)
{
    ::sockaddr_storage sa;
    ::socklen_t salen = sizeof(sa);
    if (::getpeername(peer.native(), (::sockaddr*)&sa, &salen) == -1) {
        *err = SocketTraits::lastError();
        return;
    }
    assignSockaddrToEndpoint(ep, sa);
}

void SocketFactory::localEndpoint(ErrorCode* err,
                                  Endpoint* ep,
                                  const Socket& sock)
{
    ::sockaddr_storage sa;
    ::socklen_t salen = sizeof(sa);
    if (::getsockname(sock.native(), (::sockaddr*)&sa, &salen) == -1) {
        *err = SocketTraits::lastError();
        return;
    }
    assignSockaddrToEndpoint(ep, sa);
}

std::ostream& operator<<(std::ostream& os, const Endpoint& ep)
{
    return (os << ep.address << ':' << ep.portNum);
}

} // namespace nwl
