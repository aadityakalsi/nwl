/*! sockettraits.unix.hpp */
/*!
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef NWL_SOCKETTRAITS_UNIX_HPP
#define NWL_SOCKETTRAITS_UNIX_HPP

#include "nwl/log.hpp"
#include "nwl/platform.unix.hpp"
#include <nwl/sockettraits.hpp>

#include <mutex>
#include <thread>

namespace nwl {

namespace {

std::once_flag flag;

} // namespace

void SocketTraits::initSocketUsage()
{
    std::call_once(flag, []() -> void { ::signal(SIGPIPE, SIG_IGN); });
}

void SocketTraits::close(ErrorCode* err, NativeSocket sock)
{
    if (!isValid(sock) || ::close(sock) != -1) {
        *err = OK;
    }
    else {
        *err = ERR;
    }
}

SocketTraits::SizeType SocketTraits::read(ErrorCode* err,
                                          NativeSocket sock,
                                          void* data,
                                          SizeType size)
{
    auto nread = ::read(sock, data, size);
    if (nread == -1) {
        int err_no = errno;
        if (err_no == EWOULDBLOCK || err_no == EAGAIN) {
            *err = ErrorCode(err_no, std::generic_category());
        }
        else {
            *err = ERR;
        }
        nread = 0;
    }
    else {
        *err = OK;
    }
    return nread;
}

SocketTraits::SizeType SocketTraits::write(ErrorCode* err,
                                           NativeSocket sock,
                                           const void* data,
                                           SizeType size)
{
    auto nwritten = ::write(sock, data, size);
    if (nwritten == -1) {
        int err_no = errno;
        if (err_no == EWOULDBLOCK || err_no == EAGAIN) {
            *err = ErrorCode(err_no, std::generic_category());
        }
        else {
            *err = ERR;
        }
        nwritten = 0;
    }
    else {
        *err = OK;
    }
    return nwritten;
}

void SocketTraits::shutDown(ErrorCode* err,
                            NativeSocket sock,
                            ShutDownKind::Type kind)
{
    if (::shutdown(sock, static_cast<int>(kind)) == -1) {
        *err = ERR;
    }
    else {
        *err = OK;
    }
}

void SocketTraits::setBlocking(ErrorCode* err, NativeSocket sock, bool block)
{
    int flags = ::fcntl(sock, F_GETFL);
    if (flags == -1) {
        *err = ERR;
        return;
    }
    if (block) {
        flags &= ~O_NONBLOCK;
    }
    else {
        flags |= O_NONBLOCK;
    }
    if (::fcntl(sock, F_SETFL, flags) == -1) {
        *err = ERR;
    }
    else {
        *err = OK;
    }
}

void SocketTraits::setKeepAlive(ErrorCode* err,
                                NativeSocket sock,
                                Second kaTime)
{
    int val = 1;
    if (::setsockopt(sock, SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val)) ==
        -1) {
        *err = ERR;
        return;
    }
#ifdef __linux__
    if (::setsockopt(sock, IPPROTO_TCP, TCP_KEEPIDLE, &val, sizeof(val)) ==
        -1) {
        *err = ERR;
        return;
    }
    kaTime /= 3;
    kaTime = kaTime == 0 ? 1 : kaTime;
    if (::setsockopt(
          sock, IPPROTO_TCP, TCP_KEEPINTVL, &kaTime, sizeof(kaTime)) == -1) {
        *err = ERR;
        return;
    }
    if (::setsockopt(sock, IPPROTO_TCP, TCP_KEEPCNT, &val, sizeof(val)) ==
        -1) {
        *err = ERR;
        return;
    }
#else
    (void)kaTime;
#endif
    *err = OK;
}

void SocketTraits::setNoDelay(ErrorCode* err, NativeSocket sock, bool noDelay)
{
    int val = noDelay ? 1 : 0;
    if (::setsockopt(sock, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)) ==
        -1) {
        *err = ERR;
        return;
    }
    *err = OK;
}

void SocketTraits::setSendTimeout(ErrorCode* err,
                                  NativeSocket sock,
                                  MilliSecond timeout)
{
    ::timeval tv;
    tv.tv_sec = timeout / 1000;
    tv.tv_usec = (timeout % 1000) * 1000;
    if (::setsockopt(sock, SOL_SOCKET, SO_SNDTIMEO, &tv, sizeof(tv)) == -1) {
        *err = ERR;
        return;
    }
    *err = OK;
}

ErrorCode SocketTraits::lastError()
{
    return ERR;
}

bool SocketTraits::isBlockingError(const ErrorCode& err)
{
    if (&err.category() != &std::generic_category())
        return false;
    switch (err.value()) {
    case EINPROGRESS:
#if EAGAIN == EWOULDBLOCK
    case EAGAIN:
#else
    case EAGAIN:
    case EWOULDBLOCK:
#endif
    case EINTR:
        return true;
    default:
        return false;
    }
}

} // namespace nwl

#endif /*NWL_SOCKETTRAITS_UNIX_HPP*/
