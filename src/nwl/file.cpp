/*! file.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#include <nwl/file.hpp>
#include <nwl/filetraits.hpp>

namespace nwl {

File::File() noexcept
: d_fd(InvalidFile)
{
}

File::~File() noexcept
{
    ErrorCode err;
    close(&err);
    // if (err) {
    //     LOG << "closing file: " << fd << "; err: " << err.message() <<
    //     "\n";
    // }
}

File::File(File&& rhs) noexcept
: d_fd(rhs.d_fd)
{
    rhs.d_fd = InvalidFile;
}

File& File::operator=(File&& rhs) noexcept
{
    if (this != &rhs) {
        ErrorCode err;
        close(&err);
        d_fd = rhs.d_fd;
        rhs.d_fd = InvalidFile;
    }
    return *this;
}

// ACCESSORS
bool File::valid() const
{
    return FileTraits::isValid(d_fd);
}
NativeFile File::native() const
{
    return d_fd;
}

File::SizeType File::write(ErrorCode* err,
                           const void* data,
                           SizeType count) const
{
    return FileTraits::write(err, d_fd, data, count);
}

File::SizeType File::read(ErrorCode* err, void* data, SizeType count) const
{
    return FileTraits::read(err, d_fd, data, count);
}

File::OffsetType File::tell(ErrorCode* err) const
{
    return FileTraits::tell(err, d_fd);
}

void File::seek(ErrorCode* err, File::OffsetType off) const
{
    FileTraits::seek(err, d_fd, off);
}

// MANPIULATORS
void File::close(ErrorCode* err)
{
    FileTraits::close(err, d_fd);
    if (!*err) {
        d_fd = InvalidFile;
    }
}

File::File(NativeFile fd) noexcept
: d_fd(fd)
{
}

} // namespace nwl
