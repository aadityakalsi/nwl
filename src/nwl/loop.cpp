/*! loop.cpp */
/*
 * Copyright 2018 Aaditya Kalsi (akalsi.dev@gmail.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 * notice, this list of conditions and the following disclaimer in the
 * documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef _BSD_SOURCE
#    define _BSD_SOURCE
#endif

#if defined(__linux__) || defined(UNIX) || defined(__unix__) || \
  defined(__APPLE__)
#    ifndef _GNU_SOURCE
#        define _GNU_SOURCE
#    endif
#    ifndef _XOPEN_SOURCE
#        define _XOPEN_SOURCE 700
#    endif
#    define _LARGEFILE_SOURCE
#    define _FILE_OFFSET_BITS 64
#endif

#ifdef __linux__
#    include <sys/epoll.h>
#    include <unistd.h>
#    define USE_EPOLL
#endif

#ifdef __APPLE__
#    include "nwl/platform.unix.hpp"
#    include <AvailabilityMacros.h>
#    include <sys/event.h>
#    include <sys/time.h>
#    include <sys/types.h>
#    define USE_KQUEUE
#endif

#ifdef _WIN32
#    include "nwl/platform.win.hpp"
#    include <algorithm>
#    define USE_SELECT
#endif

#include <nwl/loop.hpp>

#include <atomic>
#include <chrono>
#include <map>
#include <vector>

#include "nwl/log.hpp"

namespace nwl {

static const int DEFAULT_TIMEOUT_MS = 25;

static_assert(sizeof(Socket) == sizeof(NativeSocket),
              "To keep sockets in event polls.");

namespace {

static const SizeType ALIGN = 16;
static const SizeType ALIGN_MASK = ALIGN - 1;

struct MemoryChunk
{
    MemoryChunk* prev;
    char* curr;
    char* end;

    void* alloc(SizeType sz)
    {
        sz = (sz + ALIGN_MASK) & ~SizeType(ALIGN_MASK);
        if (sz == 0 || curr + sz > end) {
            return nullptr;
        }
        void* mem = curr;
        curr += sz;
        return mem;
    }

    static MemoryChunk* makeNew(SizeType sz, MemoryChunk* prev)
    {
        sz += sizeof(MemoryChunk);
        sz = (sz + ALIGN_MASK) & ~SizeType(ALIGN_MASK);
        void* mem = ::operator new(sz, std::nothrow);
        if (!mem) {
            return nullptr;
        }
        auto chunk = new (mem) MemoryChunk;
        chunk->prev = prev;
        chunk->curr = reinterpret_cast<char*>(chunk) + sizeof(MemoryChunk);
        chunk->end = reinterpret_cast<char*>(chunk) + sz;
        return chunk;
    }

    static void destroy(MemoryChunk* chunk) { ::operator delete(chunk); }
};

struct FreeNode
{
    FreeNode* prev;
    FreeNode* next;

    void unlink()
    {
        assert(next->prev == this);
        assert(prev->next == this);
        prev->next = next;
        next->prev = prev;
    }

    void link(FreeNode* prev, FreeNode* next)
    {
        assert(prev->next == next);
        assert(next->prev == prev);
        this->next = next;
        next->prev = this;
        prev->next = this;
        this->prev = prev;
    }
};

class PoolAllocatorImpl
{
  private:
    FreeNode d_freeHead;
    MemoryChunk* d_chunkHead;
    SizeType d_elSize;

    SizeType chunkSize() { return nodeSize() * 512; }

    SizeType nodeSize()
    {
        return d_elSize < sizeof(FreeNode) ? sizeof(FreeNode) : d_elSize;
    }

  public:
    PoolAllocatorImpl(SizeType sz) noexcept
    : d_freeHead()
    , d_chunkHead(nullptr)
    , d_elSize(sz)
    {
        d_freeHead.next = &d_freeHead;
        d_freeHead.prev = &d_freeHead;
    }

    ~PoolAllocatorImpl() noexcept
    {
        auto chunk = d_chunkHead;
        while (chunk) {
            auto prev = chunk->prev;
            MemoryChunk::destroy(chunk);
            chunk = prev;
        }
    }

    void* allocate(SizeType n, SizeType elSize)
    {
        assert(n == 1);
        assert(elSize <= d_elSize);
        if (d_freeHead.next != &d_freeHead) {
            auto node = d_freeHead.next;
            node->unlink();
            return node;
        }
        auto currChunk = d_chunkHead;
        if (currChunk) {
            auto node = currChunk->alloc(nodeSize());
            if (node) {
                return node;
            }
        }
        currChunk = MemoryChunk::makeNew(chunkSize(), currChunk);
        if (!currChunk) {
            throw std::bad_alloc();
        }
        d_chunkHead = currChunk;
        auto node = currChunk->alloc(nodeSize());
        assert(node);
        return node;
    }

    void deallocate(void* node) noexcept
    {
        if (!node) {
            return;
        }
        FreeNode* freeNode = reinterpret_cast<FreeNode*>(node);
        freeNode->link(&d_freeHead, d_freeHead.next);
    }
};

template<class T>
class PoolAllocator
{
  private:
    std::shared_ptr<PoolAllocatorImpl> d_impl;

  public:
    using value_type = T;

    template<class U>
    friend class PoolAllocator;

    PoolAllocator() noexcept
    : d_impl(std::make_shared<PoolAllocatorImpl>(sizeof(T)))
    {
    }

    template<class U>
    PoolAllocator(const PoolAllocator<U>& rhs) noexcept
    : d_impl(rhs.d_impl)
    {
    }

    ~PoolAllocator() noexcept = default;

    value_type* allocate(SizeType n)
    {
        return static_cast<value_type*>(d_impl->allocate(n, sizeof(T)));
    }

    void deallocate(value_type* node, SizeType) noexcept
    {
        d_impl->deallocate(node);
    }
};

template<class T, class U>
bool operator==(PoolAllocator<T> const& x, PoolAllocator<U> const& y) noexcept
{
    return x.d_impl == y.d_impl;
}

template<class T, class U>
bool operator!=(PoolAllocator<T> const& x, PoolAllocator<U> const& y) noexcept
{
    return !(x == y);
}

} // namespace

struct Fired
{
    NativeSocket fd;
    SocketEvent::Type events;
};

struct TimerEventData
{
    TimerObserver* observer;
    TimerToken id;

    void handleCallback(Loop* l) const { observer->handleEvent(l, id); }
};

struct Loop::Impl
{
    std::vector<Socket> d_sockets;
    std::vector<SocketEvent::Type> d_sockEvents;
    std::vector<SocketObserver*> d_sockListeners;
    std::vector<Fired> d_fired;
    NativeSocket d_maxFd;
    std::atomic<int> d_quit;

    using TimePoint = std::chrono::high_resolution_clock::time_point;

    static TimePoint now()
    {
        return std::chrono::high_resolution_clock::now();
    }

    using TimerMapAlloc =
      PoolAllocator<std::pair<const TimePoint, TimerEventData>>;
    using TimerMap = std::multimap<TimePoint,
                                   TimerEventData,
                                   std::less<TimePoint>,
                                   TimerMapAlloc>;

    using TimerIdToIterAlloc =
      PoolAllocator<std::pair<const TimerToken, TimerMap::iterator>>;
    using TimerIdToIter = std::map<TimerToken,
                                   TimerMap::iterator,
                                   std::less<TimerToken>,
                                   TimerIdToIterAlloc>;

    TimerMap d_timers;
    TimerIdToIter d_timerIdToIter;
    TimePoint d_now;
    std::vector<TimerEventData> d_timerDataTemp;
    TimerToken d_lastId;

    void init(size_t size)
    {
        d_sockets.resize(size);
        d_sockEvents.resize(size, SocketEvent::None);
        d_sockListeners.resize(size, nullptr);
        d_fired.resize(size);
        d_maxFd = 0;
        initImpl(size);
        d_quit = 0;
        d_now = now();
        d_lastId = 0;
    }

    ~Impl() { destroyImpl(); }

    void resize(size_t size)
    {
        d_sockets.resize(size);
        d_sockEvents.resize(size, SocketEvent::None);
        d_sockListeners.resize(size, nullptr);
        d_fired.resize(size);
        resizeImpl(size);
    }

    bool addSocketEvent(Socket&& s,
                        SocketObserver* l,
                        SocketEvent::Type events)
    {
        assert(s.native() <= (NativeSocket)d_sockets.size());
        const NativeSocket fd = s.native();
        NativeSocket prevMaxFd = d_maxFd;
        if (fd > d_maxFd) {
            d_maxFd = fd;
        }
        if (!addSocketEvent(fd, events)) {
            d_maxFd = prevMaxFd;
            return false;
        }
        d_sockets[fd] = std::forward<Socket>(s);
        d_sockListeners[fd] = l;
        d_sockEvents[fd] = events;
        return true;
    }

    Socket removeSocketEvent(SocketToken id, bool _unused)
    {
        NativeSocket fd = id;
        if (!removeSocketEvent(fd)) {
            return Socket();
        }
        d_sockListeners[fd] = nullptr;
        d_sockEvents[fd] = SocketEvent::None;
        return std::move(d_sockets[fd]);
    }

  private:
    void handleTimers(Loop* l)
    {
        auto end = d_timers.lower_bound(d_now);
        auto curr = d_timers.begin();
        d_timerDataTemp.clear();
        for (; curr != end; ++curr) {
            d_timerDataTemp.push_back(curr->second);
        }
        for (auto entry : d_timerDataTemp) {
            entry.handleCallback(l);
        }
        for (auto entry : d_timerDataTemp) {
            l->deregisterTimeEvent(entry.id);
        }
    }

    void fillTimeout(::timeval* tv)
    {
        if (d_timers.empty()) {
            tv->tv_sec = 0;
            tv->tv_usec = DEFAULT_TIMEOUT_MS * 1000;
        }
        else {
            auto diff = d_timers.begin()->first - d_now;
            auto diffUs =
              std::chrono::duration_cast<std::chrono::microseconds>(diff);
            tv->tv_sec = static_cast<long>(diffUs.count() / 1000000);
            tv->tv_usec = diffUs.count() % 1000000;
        }
    }

  public:
    void run(Loop* l, ErrorCode* err)
    {
        ::timeval tv;
        d_quit = 0;
        while (!d_quit) {
            d_now = now();
            handleTimers(l);
            fillTimeout(&tv);
            int nEvents = poll(err, &tv);
            for (int i = 0; i < nEvents; ++i) {
                const auto& ev = d_fired[i];
                auto listener = d_sockListeners[ev.fd];
                listener->handleEvent(l, d_sockets[ev.fd], ev.events);
            }
        }
    }

    void stop() { d_quit = 1; }

#ifdef USE_EPOLL
    int epollFd;
    std::vector<::epoll_event> events;

    void initImpl(size_t sz)
    {
        events.resize(sz);
        epollFd = ::epoll_create(sz == 0 ? 1024 : sz);
        assert(epollFd != -1);
    }

    void destroyImpl() { ::close(epollFd); }

    void resizeImpl(size_t sz) { events.resize(sz); }

    bool addSocketEvent(int fd, SocketEvent::Type events)
    {
        assert(fd <= d_maxFd);
        assert(fd < (NativeSocket)d_sockets.size());
        SocketEvent::Type curr = d_sockEvents[fd];
        int epollOp =
          curr == SocketEvent::None ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
        ::epoll_event ev;
        std::memset(&ev, 0, sizeof(ev));
        ev.events = 0;
        ((int&)curr) |= events;
        if (curr & SocketEvent::Read) {
            ev.events |= EPOLLIN;
        }
        if (curr & SocketEvent::Write) {
            ev.events |= EPOLLOUT;
        }
        ev.data.fd = fd;
        if (::epoll_ctl(epollFd, epollOp, fd, &ev) == -1) {
            return false;
        }
        return true;
    }

    bool removeSocketEvent(int fd)
    {
        ::epoll_event ev;
        ev.events = 0;
        ev.data.fd = fd;
        return ::epoll_ctl(epollFd, EPOLL_CTL_DEL, fd, &ev) != -1;
    }

    int poll(ErrorCode* err, ::timeval* tv)
    {
        int numEvents = 0;
        int timeout =
          tv ? (tv->tv_sec * 1000 + tv->tv_usec / 1000) : DEFAULT_TIMEOUT_MS;
        int rv = ::epoll_wait(epollFd, &events[0], events.size(), timeout);
        if (rv < 0) {
            *err = ERR;
            return numEvents;
        }
        numEvents = rv;
        for (int i = 0; i < numEvents; ++i) {
            const ::epoll_event& e = events[i];
            Fired& f = d_fired[i];
            f.fd = e.data.fd;
            if (e.events & EPOLLIN) {
                ((int&)f.events) |= SocketEvent::Read;
            }
            if (e.events & EPOLLOUT) {
                ((int&)f.events) |= SocketEvent::Write;
            }
            if (e.events & EPOLLERR) {
                ((int&)f.events) |= SocketEvent::Write;
            }
            if (e.events & EPOLLHUP) {
                ((int&)f.events) |= SocketEvent::Write;
            }
        }
        return numEvents;
    }
#endif

#ifdef USE_KQUEUE
    int kqueueFd;
    std::vector<struct ::kevent> events;

    void initImpl(size_t sz)
    {
        events.resize(sz);
        kqueueFd = ::kqueue();
        assert(kqueueFd != -1);
    }

    void destroyImpl() { ::close(kqueueFd); }

    void resizeImpl(size_t sz) { events.resize(sz); }

    bool addSocketEvent(int fd, SocketEvent::Type events)
    {
        assert(fd <= d_maxFd);
        struct ::kevent ev;
        std::memset(&ev, 0, sizeof(ev));
        if (events & SocketEvent::Read) {
            EV_SET(&ev, fd, EVFILT_READ, EV_ADD, 0, 0, nullptr);
            if (::kevent(kqueueFd, &ev, 1, nullptr, 0, nullptr) == -1) {
                return false;
            }
        }
        if (events & SocketEvent::Write) {
            EV_SET(&ev, fd, EVFILT_WRITE, EV_ADD, 0, 0, nullptr);
            if (::kevent(kqueueFd, &ev, 1, nullptr, 0, nullptr) == -1) {
                return false;
            }
        }
        return true;
    }

    bool removeSocketEvent(int fd)
    {
        struct ::kevent ev;
        int events = d_sockEvents[fd];
        if (events & SocketEvent::Read) {
            EV_SET(&ev, fd, EVFILT_READ, EV_DELETE, 0, 0, nullptr);
            if (::kevent(kqueueFd, &ev, 1, nullptr, 0, nullptr) == -1) {
                return false;
            }
        }
        if (events & SocketEvent::Write) {
            EV_SET(&ev, fd, EVFILT_WRITE, EV_DELETE, 0, 0, nullptr);
            if (::kevent(kqueueFd, &ev, 1, nullptr, 0, nullptr) == -1) {
                return false;
            }
        }
        return true;
    }

    int poll(ErrorCode* err, ::timeval* tv)
    {
        int numEvents = 0;
        ::timespec ts;
        ::timespec* tsPtr = &ts;
        if (tv) {
            tsPtr->tv_sec = tv->tv_sec;
            tsPtr->tv_nsec = tv->tv_usec * 1000;
        }
        else {
            tsPtr->tv_sec = 0;
            tsPtr->tv_nsec = DEFAULT_TIMEOUT_MS * 1e6;
        }
        int rv =
          ::kevent(kqueueFd, nullptr, 0, &events[0], events.size(), tsPtr);
        if (rv < 0) {
            *err = ERR;
            return numEvents;
        }
        numEvents = rv;
        for (int i = 0; i < numEvents; ++i) {
            SocketEvent::Type eventType;
            struct ::kevent& ev = events[i];
            if (ev.filter == EVFILT_READ) {
                eventType = SocketEvent::Read;
            }
            else if (ev.filter == EVFILT_WRITE) {
                eventType = SocketEvent::Write;
            }
            Fired& f = d_fired[i];
            f.fd = ev.ident;
            f.events = eventType;
        }
        return numEvents;
    }
#endif

#ifdef USE_SELECT
    void initImpl(size_t sz) {}

    void destroyImpl() {}

    void resizeImpl(size_t sz) {}

    bool addSocketEvent(NativeSocket fd, SocketEvent::Type events)
    {
        return true;
    }

    bool removeSocketEvent(NativeSocket fd) { return true; }

    int poll(ErrorCode* err, ::timeval* tv)
    {
        if (d_maxFd == 0) {
            return 0;
        }

        ::fd_set readSet;
        ::fd_set writeSet;

        ::timeval local;
        if (!tv) {
            tv = &local;
            tv->tv_sec = 0;
            tv->tv_usec = DEFAULT_TIMEOUT_MS * 1000;
        }

        FD_ZERO(&readSet);
        FD_ZERO(&writeSet);

        for (int i = 0; i < d_maxFd + 1; ++i) {
            auto ev = d_sockEvents[i];
            if (ev == SocketEvent::None) {
                continue;
            }
            NativeSocket fd = d_sockets[i].native();
            if (ev & SocketEvent::Read) {
                FD_SET(fd, &readSet);
            }
            if (ev & SocketEvent::Write) {
                FD_SET(fd, &writeSet);
            }
        }

        int rv = ::select((int)d_maxFd, &readSet, &writeSet, nullptr, tv);
        if (rv == SOCKET_ERROR) {
            *err = SocketTraits::lastError();
            return 0;
        }
        else {
            *err = OK;
        }

        for (unsigned i = 0; i < readSet.fd_count; ++i) {
            NativeSocket fd = readSet.fd_array[i];
            d_fired[i].events = SocketEvent::Read;
            d_fired[i].fd = fd;
        }
        const auto findEnd = d_fired.begin() + readSet.fd_count;
        auto newFindEnd = findEnd;
        for (unsigned i = 0; i < writeSet.fd_count; ++i) {
            NativeSocket fd = writeSet.fd_array[i];
            auto pFired = std::find_if(
              d_fired.begin(), findEnd, [fd](const Fired& f) -> bool {
                  return f.fd == fd;
              });
            if (pFired == findEnd) {
                pFired = newFindEnd;
                pFired->events = SocketEvent::None;
                newFindEnd++;
            }
            (int&)(pFired->events) |= SocketEvent::Write;
            pFired->fd = fd;
        }

        return static_cast<int>(newFindEnd - d_fired.begin());
    }
#endif
};

Loop::Loop()
: d_impl(new Impl)
{
    d_impl->init(0);
}

Loop::~Loop() {}

// MANIPULATORS
void Loop::stop()
{
    d_impl->stop();
}

// REGISTER/UNREGISTER
SocketToken Loop::registerSocket(Socket sock,
                                 SocketObserver* listener,
                                 SocketEvent::Type events)
{
    NativeSocket fd = sock.native();
    auto cap = (NativeSocket)d_impl->d_sockets.size();
    if (fd >= cap) {
        if (!resizeSocketSet(fd + 1)) {
            return InvalidSocketToken;
        }
    }
    if (d_impl->addSocketEvent(std::move(sock), listener, events)) {
        return fd;
    }
    else {
        return InvalidSocketToken;
    }
}
Socket Loop::deregisterSocket(SocketToken id)
{
    return d_impl->removeSocketEvent(id, true /* unused */);
}

TimerToken Loop::registerTimeEvent(TimerEvent::MilliSecond delay,
                                   TimerObserver* listener)
{
    TimerToken nextId = 0;
    if (!d_impl->d_timers.empty()) {
        nextId = ++(d_impl->d_lastId);
    }
    else {
        d_impl->d_lastId = 0;
    }
    try {
        auto now = d_impl->d_now;
        auto timeVal = now;
        timeVal += std::chrono::milliseconds(delay);
        assert(now != timeVal || delay == 0);
        auto it = d_impl->d_timers.emplace(
          timeVal, TimerEventData{ listener, nextId });
        try {
            d_impl->d_timerIdToIter.emplace(nextId, it);
            return nextId;
        } catch (const std::bad_alloc&) {
            d_impl->d_timers.erase(it);
        }
    } catch (const std::bad_alloc&) {
        // ignore std::bad_alloc
    }
    return InvalidTimerToken;
}
bool Loop::deregisterTimeEvent(TimerToken id)
{
    auto it = d_impl->d_timerIdToIter.find(id);
    if (it != d_impl->d_timerIdToIter.end()) {
        d_impl->d_timers.erase(it->second);
        d_impl->d_timerIdToIter.erase(it);
        return true;
    }
    else {
        return false;
    }
}

void Loop::run(ErrorCode* err)
{
    d_impl->run(this, err);
}

bool Loop::resizeSocketSet(size_t size)
{
    try {
        d_impl->resize(size);
        return true;
    } catch (...) {
        return false;
    }
}

} // namespace nwl
