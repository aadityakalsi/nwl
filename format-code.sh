#!/usr/bin/env bash
srcdir=$(readlink -f $(dirname "$0"))
cd "$srcdir"
shopt -s nullglob
clang-format -i export/nwl/*.h* src/nwl/*.c* src/nwl/*.h* tests/*.c* examples/chat-app/*.cpp
